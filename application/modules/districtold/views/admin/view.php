<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Pages</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name_np <span class=""></span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo $dis_name_en?>
                            
						</div> 
                    </div>
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name <span class=""></span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo $dis_name?>
                            
						</div> 
                    </div>
                    

                  
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>