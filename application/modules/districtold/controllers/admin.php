<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('district'); //module name is district here	
	}
	
	function index()
	{	
            
            $modulename = $this->uri->segment(2);
            $data['module_id']=$this->get_id_from_modulename($modulename);
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $data['permissions'] = $this->unserialize_role_array($group_id);
            $data['query'] = $this->get('dis_id');
//            echo '<pre>',print_r($data['query']->result()),'</pre>';
//            die();
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['dis_name'] = $this->input->post('dis_name', TRUE);			
		$data['dis_name_en'] =$this->input->post('dis_name_en',TRUE);
                $update_id = $this->input->post('update_id', TRUE);
                if(is_numeric($update_id))
                {
                        $attach = $this->get_attachment_from_db($update_id);
                        $data['attachment'] = $attach['attachment'];
                        
                }
                else
                {
                        $data['attachment'] = $this->input->post('userfile', TRUE);
                        
                }
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['dis_name'] = $row->dis_name;
                        $data['dis_name_en']=$row->dis_name_en;
                        $data['attachment'] = $row->attachment;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
         function get_data_for_view($update_id) {
            $query = $this->get_view($update_id);
            foreach ($query->result() as $row) {
                $data['dis_name'] = $row->dis_name;
                $data['dis_name_en']=$row->dis_name_en;
           
        
                    if (!isset($data)) {
                         $data = "";
                 }
                return $data;
        }
    }
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	function create()
	{       
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $update_id = $this->uri->segment(4);
            
		$submit = $this->input->post('submit', TRUE);
		if($submit=="Submit")
                    {
			//person has submitted the form
			$data = $this->get_data_from_post();
                    }
			else {
                            if (is_numeric($update_id))
                            {

                                      $data = $this->get_data_from_db($update_id);  

                                }
                        }
                        

            
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
                $all_district=$this->get_district();
                $data['all_district']=$all_district;
		$data['update_id'] = $update_id;
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}
        function get_district(){
            $this->load->model('mdl_district');
            $district = $this->mdl_district->get_district();
            return $district;
            
            
        }
                
	function delete()
	{	$this->load->model('mdl_district');
		$delete_id = $this->uri->segment(4);
                $group_id = $this->session->userdata['group_id'];//to set the permession of user group
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/district');
			}
		else
		{
                    if($group_id!=1)
                    {
                    $permissions = $this->unserialize_role_array($group_id);
                    $modulename = $this->uri->segment(2);
                    $module_id=$this->get_id_from_modulename($modulename);
                    $mystring = implode(" ",$permissions);
                    if((strpos($mystring, 'd'.$module_id))==false)
                    {
                        redirect('admin/dashboard');  
                    }
                    else
                    {
                      	$this->mdl_district->_delete($delete_id);
			redirect('admin/district');  
                    }
                    }
			$this->mdl_district->_delete($delete_id);
			redirect('admin/district');
		}				
	}
	function view()
        {
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
		$view_id = $this->uri->segment(4);
		if($group_id != 1){
				if (is_numeric($view_id))
                                    {
                                        $permissions = $this->unserialize_role_array($group_id);
                                        $modulename = $this->uri->segment(2);
                                        $module_id=$this->get_id_from_modulename($modulename);
                                        $mystring = implode(" ",$permissions);
                                        
                                        if((strpos($mystring, 'v'.$module_id))==false)
                                        {
                                            redirect('admin/dashboard');  
                                        }
                                        else
                                        {
                                         $data = $this->get_data_from_db($view_id);   
                                        }
                                      }
                                    }
                        $data = $this->get_data_from_db($view_id);
			$data['view_id'] = $view_id;
			$data['view_file'] = "admin/view";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
                }
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('dis_name_en', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('dis_name_en', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
				$data['attachment'] = $attach['attachment'];}
				$this->_update($update_id, $data);
			} else {
                                $nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				$this->_insert($data);
			}
			
			redirect('admin/district');
		}
			
	}
	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/district/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_district');
	$id = $this->mdl_district->get_id();
	return $id;
	}	

	function get($order_by){
	$this->load->model('mdl_district');
	$query = $this->mdl_district->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_district');
	$query = $this->mdl_district->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_district');
	$this->mdl_district->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_district');
	$this->mdl_district->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_district');
	$this->mdl_district->_delete($id);
	}
        function unserialize_role_array($group_id){		
	$this->load->model('permissions/mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
        function get_id_from_modulename($modulename){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
	return $query;
        }
}