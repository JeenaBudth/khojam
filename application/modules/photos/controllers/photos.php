<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class photos extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_photos');
		parent::__construct();
	}


	function index()
        {
            $photos = $this->get('id');
            $data['photos'] = $photos;
            $data['view_file']="photo_table";
            $this->load->module('template');
            $this->template->front($data);
	}
	function get($order_by){
	$this->load->model('mdl_photos');
	$query = $this->mdl_photos->get($order_by);
	return $query;
	}
	function details()
	{ 
                $slug = $this->uri->segment(3);
                $photos = $this->mdl_photos->get_photos($slug);
                $data['picture'] = $photos;
                $data['view_file']="form";
                $this->load->module('template');
                $this->template->front($data);
	}
       
}