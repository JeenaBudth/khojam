<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_groups extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_users_groups";
	return $table;
	} 

	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	
	
	$insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
	$permission_table = 'up_permissions';	
	$this->db->insert($permission_table, $insert_null_permission_array);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_users_groups'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_modules')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_users_groups')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}

}