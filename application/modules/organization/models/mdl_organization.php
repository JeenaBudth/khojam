<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_organization extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_table() {
        $table = "up_organization";
        return $table;
    }

//    function get($order_by) {
//        $table = $this->get_table();
//        $this->db->select('up_organization.*');
//        $this->db->select('district.dis_name_en as district');
////        $this->db->select('district.name_en as district_id');
//        $this->db->join('district', 'district.dis_id = up_organization.district_id');
//        $this->db->order_by($order_by);
//        $query = $this->db->get($table);
//        
//        return $query;
//    }
    function get($order_by)
    {
        $table=$this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->select('district.dis_name_en as district');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $this->db->order_by($order_by);
        $query=$this->db->get($table);
        return $query;
    }
    function get_location_info($cat_id, $dis_id, $limit,$offset)
    {
//        $result = $this->db->query('SELECT `org_ratings`.*, round(avg(org_ratings.rating),2)as org_rating, `org_ratings`.*, count(org_ratings.rating)as count_rating FROM (`org_ratings`) WHERE `category_id` = '.$cat_id.' AND `district_id` = '.$dis_id.' GROUP BY `org_ratings`.`id` LIMIT 5');
        $table='org_ratings';
        $this->db->select('org_ratings.*,avg(org_ratings.rating)as org_rating,count(org_ratings.rating)as count_rating');
//        $this->db->select('');
        $this->db->where('category_id',$cat_id);
        $this->db->where('district_id',$dis_id);
           
         $this->db->group_by('org_ratings.id');
        $query=$this->db->get($table, $limit, $offset);
//        var_dump($query->result());die('df');
         return $query;
//        return $result;
    }
     function get_location_info_search($organization, $location, $limit,$offset)
     {
          if($organization != NULL && $location != NULL){
             
        $table = 'org_ratings';
        $this->db->select('org_ratings.*,avg(org_ratings.rating)as org_rating ');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
      
        
       
        
        if($organization != NULL){
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('org_ratings.name', $org);
            $this->db->or_like('prop_name', $org);
            
        }
        }
        if($location != NULL){

        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
        }
        
         $this->db->join('up_category','up_category.id=org_ratings.category_id');
        $this->db->join('district','district.dis_id=org_ratings.district_id');
      
        $this->db->group_by('org_ratings.id');
        $query = $this->db->get($table, $limit, $offset);
//        var_dump($query->result());die('sf');
        return $query;
        } 
        else if($organization == NULL && $location != NULL){
            $table ='org_ratings';
        $this->db->select('org_ratings.*,avg(org_ratings.rating)as org_rating ');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
        
       
        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
     
         $this->db->join('up_category','up_category.id=org_ratings.category_id');
        $this->db->join('district','district.dis_id=org_ratings.district_id');
         $this->db->group_by('org_ratings.id');
        $query = $this->db->get($table, $limit, $offset);
        
        return $query;
        }else if($organization != NULL && $location == NULL){
            $table = 'org_ratings';
         $this->db->select('org_ratings.*,avg(org_ratings.rating)as org_rating ');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
        
       
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('org_ratings.name', $org);
            $this->db->or_like('prop_name', $org);
           
        }
        
         $this->db->join('up_category','up_category.id=org_ratings.category_id');
        $this->db->join('district','district.dis_id=org_ratings.district_id');
         $this->db->group_by('org_ratings.id');
        $query = $this->db->get($table,$limit, $offset);
       
        return $query;
        }else{
            return Null;
        }
     }

    function get_user_search($organization, $location, $limit, $offset) {
        if($organization != NULL && $location != NULL){
        $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        
        if($organization != NULL){
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('up_organization.name', $org);
            $this->db->or_like('prop_name', $org);
            $this->db->or_like('meta_tag', $org);
        }
        }
        if($location != NULL){
        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
        }
        
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
          $query = $this->db->get($table, $limit, $offset);
        return $query;
        } 
        else if($organization == NULL && $location != NULL){
            $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
     
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $query = $this->db->get($table, $limit, $offset);
        return $query;
        }else if($organization != NULL && $location == NULL){
            $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('up_organization.name', $org);
            $this->db->or_like('prop_name', $org);
        }
        
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $query = $this->db->get($table, $limit, $offset);
        return $query;
        }else{
            return Null;
        }
        
//        $table = $this->get_table();
//        $this->db->select('up_organizations.*');
//        $this->db->select('up_category.name as category');
//        $this->db->select('up_category.slug as category_slug');
//        $this->db->select('district.dis_name_en as district');
//         $this->db->join('up_category','up_category.id=up_organization.category_id');
//         $this->db->join('district','district.dis_id=up_organization.district_id');
//         $this->db->or_like('up_organization.name', $organization);
//          $this->db->or_like('up_organization.map_location', $location);
//        $query = $this->db->get($table);
//        return $query;
    }
function calculate_rows($organization, $location) {
        if($organization != NULL && $location != NULL){
        $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        
        if($organization != NULL){
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('up_organization.name', $org);
            $this->db->or_like('prop_name', $org);
        }
        }
        if($location != NULL){
        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
        }
        
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $query = $this->db->get($table);
        return $query;
        
        } 
        else if($organization == NULL && $location != NULL){
            $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        $locArr = explode(' ',$location);
//        print_r($locArr); die;
        foreach($locArr as $locate){
            $this->db->or_like('district.dis_name_en', $locate);
            $this->db->or_like('map_location', $locate);
        }
     
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $query = $this->db->get($table);
        return $query;
        }else if($organization != NULL && $location == NULL){
            $table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->select('up_category.name as category');
        $this->db->select('up_category.slug as category_slug');
        $this->db->select('district.dis_name_en as district');
       
        $orgname = explode(' ', $organization);
        foreach($orgname as $org){
            $this->db->or_like('up_category.name', $org);
            $this->db->or_like('up_organization.name', $org);
            $this->db->or_like('prop_name', $org);
        }
        
         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $query = $this->db->get($table);
        return $query;
        }else{
            return Null;
}

        }
    function get_where($id) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        return $query;
    }

    function _insert($data) {
        $table = $this->get_table();
        $this->db->insert($table, $data);
    }

    function _update($id, $data) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }

    function _delete($id) {
        $table = $this->get_table();
        $this->db->where('id', $id);
        $this->db->delete($table);
        $this->delete_permissions_of_that_module_too($id);
    }

    function delete_permissions_of_that_module_too($id) {
        $table = 'up_permissions';
        $query = $this->db->get($table)->result();
        echo '<pre>';
        foreach ($query as $row) {
            $roles_array = unserialize($row->roles);
            foreach ($roles_array as $check) {
                if ($check == $id) {
                    unset($roles_array[$id]);
                    $new_role = serialize($roles_array);
                    $this->db->where('id', $row->id);
                    $this->db->update($table, array('roles' => $new_role));
                }
            }
        }
    }
function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_organization'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
   
   
    function get_module_ids() {
        $this->db->select('id');
        $this->db->order_by('id');
        $dropdowns = $this->db->get('up_organization')->result();
        foreach ($dropdowns as $dropdown) {
            $dropdownlist[$dropdown->id] = $dropdown->id;
        }
        if (empty($dropdownlist)) {
            return NULL;
        }
        $finaldropdown = $dropdownlist;
        return $finaldropdown;
    }

    function get_id_from_modulename($modulename) {
        $table = $this->get_table();
        $this->db->select('id');
        $this->db->where('slug', $modulename);
        $query = $this->db->get($table)->result();
        $result = $query[0]->id;
        return $result;
    }

     function get_id_from_slug($slug) {
//        $table = $this->get_table();
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_category')->result();
       
        $result = $query[0]->id;
        
        return $result;
    }
    
     function get_id_from_slugs($slug) {
//        $table = $this->get_table();
        $this->db->select('*');
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_category')->result_array();
//        var_dump($query);die;
//        $result = $query[0]->id;
        return $query;
    }
         function get_id_from_slug_cat($slug) {
//        $table = $this->get_table();
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_category')->result();
        $result = $query[0]->id;
        return $result;
    }
         function get_id_from_slug_org($slug) {
//        $table = $this->get_table();
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $query = $this->db->get('up_organization')->result();
        $result = $query[0]->id;
        return $result;
    }
       
    function get_all_slug_from_module() {
        $table = $this->get_table();
        $this->db->select('slug');
        $query = $this->db->get($table)->result();
        $i = 1;
        foreach ($query as $row) {
            $new_array[$i] = $row->slug;
            $i++;
        }
        return $new_array;
    }

    function get_slug_from_moduleid($module_id) {
        $table = $this->get_table();
        $this->db->select('slug');
        $this->db->where('id', $module_id);
        $query = $this->db->get($table)->result();
        $result = $query[0]->slug;
        return $result;
    }
    function get_org_name($cat_id,$dis_id){ 
//       $query= $this->db->query("select name, avg(rating)as org_rating,slug,phone_no,email,establish, website, attachment2,map_location,description  from (SELECT O.id, O.name, R.rating,O.slug,O.phone_no,O.email,O.establish, O.category_id, O.website, O.attachment2,O.map_location,O.description FROM up_organization as O left outer join up_rating as R on O.id=R.org_id ) as A where A.category_id=$id group by A.id  ");
          $table = $this->get_table();
        
         $this->db->where('category_id', $cat_id);
         $this->db->where('district_id', $dis_id);
         $query =$this->db->count_all_results($table);
        return $query;
    }
    function get_district_name($id){
        $table=$this->get_table();
        $this->db->select('district.*');
        $this->db->select('district.dis_name_en as district');
        $this->db->join('district','district.dis_id=up_organization.district_id');
        $this->db->where('category_id',$id);
        $this->db->group_by('district_id');
        $query=$this->db->get($table)->result_array();
        return $query;
        
                
        
    }
    function get_view_details($cat_id, $org_id)
    
    {
//        $result = $this->db->query('SELECT `org_ratings`.*, round(avg(org_ratings.rating),2)as org_rating, `org_ratings`.*, count(org_ratings.rating)as count_rating, `district`.`dis_name_en` as district FROM (`org_ratings`) JOIN `district` ON `district`.`dis_id`=`org_ratings`.`district_id` WHERE `category_id` = '.$cat_id.' AND `id` = '.$org_id.' GROUP BY `org_ratings`.`id`');
////        var_dump($result->result());die('ff');
//        return $result;
//       $table=$this->get_table();
//        $this->db->select('up_organization.*');
//        $this->db->select('up_services.name as services');
//        $this->db->join('up_services','up_services.id=up_organization.services');
         $table='org_ratings';
        $this->db->select('org_ratings.*,avg(org_ratings.rating)as org_rating,count(org_ratings.rating)as count_rating');

        $this->db->select('district.dis_name_en as district');
        $this->db->join('district','district.dis_id=org_ratings.district_id');
//        $this->db->select('attachment1');
         $this->db->where('category_id',$cat_id);
         $this->db->where('id',$org_id);
          $this->db->group_by('org_ratings.id');
        $query=$this->db->get($table)->result_array();
//        var_dump($query);die('cfv');
//        print_r($query);
//        die();
        return $query; 
    }
    
    function get_organization($name){
        $table = $this->get_table();
        $this->db->select('up_organization.name');
//        $this->db->select('up_category.name as category');
//         $this->db->join('up_category','up_category.id=up_organization.category_id');
        $this->db->or_like('up_organization.name', $name);
//        $this->db->or_like('up_category.name', $name);
        $query = $this->db->get($table);
        if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
        $row_set[] = htmlentities(stripslashes($row['name'])); //build an array
      }
      echo json_encode($row_set); //format the array into json data
    }
    }
    function get_location($name){ 
        $table = $this->get_table();
        $this->db->select('up_organization.map_location');
        $this->db->or_like('up_organization.map_location', $name);
        $query = $this->db->get($table);
//        print_r($query->result()); die;
        if($query->num_rows > 0){
      foreach ($query->result_array() as $row){
        $row_set[] = htmlentities(stripslashes($row['map_location'])); //build an array
      }
      echo json_encode($row_set); //format the array into json data
    }
    
    }
        function get_org_id($organization)
    {
        $table = $this->get_table();
        $this->db->where('slug', $organization);
        $query = $this->db->get($table);
        return $query->result_array();
    }
    function get_id_district_slug($district)
    {
        $this->db->select('dis_id');
      $this->db->where('dis_name_en', $district);
        $query = $this->db->get('district')->result();
        $result = $query[0]->dis_id;
        return $result;
    }
     function get_org_name_limited($cat_id, $dis_id, $limit,$offset)
     {
         $query= $this->db->query("select name, avg(rating)as org_rating,slug,phone_no,email,establish, website, attachment2,map_location,description  from (SELECT O.id, O.name, R.rating,O.slug,O.phone_no,O.email,O.establish, O.category_id,O.district_id, O.website, O.attachment2,O.map_location,O.description FROM up_organization as O left outer join up_rating as R on O.id=R.org_id ) as A where A.category_id=$cat_id and A.district_id=$dis_id group by A.id   LIMIT $offset,$limit");
//         $table = $this->get_table();
//         $this->db->where('category_id', $cat_id);
//         $this->db->where('district_id', $dis_id);
////         $this->db->join('up_category','up_category.id=up_organization.category_id');
////        $this->db->join('district','district.dis_id=up_organization.district_id');
//        $query = $this->db->get($table, $limit, $offset);
//        var_dump($query->result_array()); die;
        return $query->result_array();
     }
     function get_groups_dropdown()
	{
		$this->db->select('id, name');
                //$this->db->where('id > 1');
		$this->db->order_by('id','AESC');
		$dropdowns = $this->db->get('up_organization')->result();
		foreach ($dropdowns as $dropdown)
		{
                $dropdownlist[0] = '-- Select--';    
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
        
        function get_organization_dropdown($cat_id,$dis_id){
	$table = $this->get_table();
        $this->db->select('up_organization.*');
        $this->db->where('category_id', $cat_id);
        $this->db->where('district_id', $dis_id);
	$query=$this->db->get($table);
	return $query;
	}
        function get_organization_ajax($cat_id,$dis_id){
	$table = $this->get_table();
        $this->db->select('id,name');
        $this->db->where('category_id', $cat_id);
        $this->db->where('district_id', $dis_id);
	$query=$this->db->get($table);
	return $query;
	}
     
    
}
