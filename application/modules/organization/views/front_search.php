
<script
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script>

    info_win = {
        show: false,
        content: "",
        marker: new google.maps.Marker()
    };
    distance = [];
    infowindows = new google.maps.InfoWindow();
    path = new google.maps.MVCArray();
    function initialize() {
        var map = new google.maps.Map(document.getElementById("googleMap"), {zoom: 14});
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                bounds.extend(new google.maps.LatLng(pos.lat, pos.lng));
// 	      infoWindow.setPosition(pos);
// 	      infoWindow.setContent('Location found.');
// 	      infoWindow.open(map);

                var currPosMarker = new google.maps.Marker({animation: google.maps.Animation.BOUNCE});
                currPosMarker.setPosition(pos);
                currPosMarker.setMap(map);
                currPosMarker.addListener('mouseover', function () {
                    infowindow.setContent('<b>You are here!<b>');
                    infowindow.open(map, currPosMarker);
                });
                currPosMarker.addListener('mouseout', function () {
                    infowindow.close(map);
                });
                map.setCenter(pos);
                path.push(pos);
                count = 0;
                var marker;


<?php $c1 = 0;
foreach ($location_info->result() as $row) {
    ?>

                    //***************************DISTANCE STUFFS***********************************//
                    var origin = new google.maps.LatLng(pos.lat, pos.lng);
                    var destination = new google.maps.LatLng(<?php echo $row->latitude; ?>,<?php echo $row->longitude; ?>);
                    getDestinationInfo(map, origin, destination, count, info_win);
                    // alert( distanceInfo.distance);
                    //*************************************************************//


                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $row->latitude; ?>,<?php echo $row->longitude; ?>),
                    });
                    marker.setMap(map);

                    google.maps.event.addListener(marker, 'click', (function (marker) {


                        return function () {
                            var directionsService = new google.maps.DirectionsService;
                            var directionsDisplay = new google.maps.DirectionsRenderer;

                            directionsDisplay.setMap(map);


                            calculateAndDisplayRoute(directionsService, directionsDisplay, origin, new google.maps.LatLng(<?php echo $row->latitude; ?>,<?php echo $row->longitude; ?>));


                            info_win.content = '<a href="<?php echo base_url() . $row->slug; ?>"><?php echo $row->name; ?></a>';
                            info_win.show = true;
                            info_win.marker = marker;
                            getDestinationInfo(map, origin, destination,<?php echo $c1; ?>, info_win);

                        }
                    })(marker));

    // 	      		marker.addListener( 'mouseout', function() {
    // 	      			  infowindows.close(map);

    // 	      			  });

                    count++;
                    bounds.extend(marker.position);
                    map.fitBounds(bounds);
    <?php $c1++;
} ?>
                //map.fitBounds(latlngbounds);
                //alert(count);
            }, function () {
                //GeoLocation Error!!!
                map.setZoom(14);
                map.setCenter(new google.maps.LatLng(27.698268, 85.325289));
            });
        } else {
            // Browser doesn't support Geolocation
            map.setZoom(14);
            map.setCenter(new google.maps.LatLng(27.698268, 85.325289));
        }


    }
    function getDestinationInfo(map, origin, destination, index, info_win)
    {
        //***************************DISTANCE STUFFS***********************************//
        if (!info_win.show) {
            service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix(
                    {
                        origins: [origin],
                        destinations: [destination],
                        travelMode: google.maps.TravelMode.DRIVING,
                        avoidTolls: true
                    },
            callback
                    );

            function callback(response, status) {
                if (status == "OK") {


                    // alert( this.distanceInfo.distance+index);

                    distance[index] = response.rows[0].elements[0].distance.text;
                    var currAddress = response.originAddresses[0];
                    var content = '<p>' + distance[index] + ' from your location(' + currAddress + ')</p>';
                    document.getElementById("dist_info" + index).innerHTML = content;




                } else {
                    alert("Error: " + status);
                }
            }


        }
        else
        {
            info_win.show = false;
            //alert(index);
            var content = info_win.content + '<br><p>' + distance[index] + ' from your location</p>';
            infowindows.setContent(content);
            infowindows.open(map, info_win.marker);

        }


        //*************************************************************//
    }
    /* road showing function*/
    function calculateAndDisplayRoute(directionsService, directionsDisplay, origin, destination) {
        directionsService.route({
            origin: origin,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php
// var_dump($day);die;
if ($day != '') {
    foreach ($day as $key => $value) {
        ${'working_id_' . $key} = $value;
    }
}
?>


<!--<!DOCTYPE html>
<html lang="en">
 
    <body class="sp-profile-page">
<nav class="navbar navbar-default navbar-fixed-top sp-navbar-top" role="navigation" >
            <div class="container-fluid">
                <div class="row row-offcanvas row-offcanvas-right">
                    <div class="container-fluid">
                        <div class="navbar-header col-lg-2">
                            <div class="logo">
                                <img src="<?php echo base_url(); ?>/design/frontend/img/logo.png" style="padding: 3px 0px; width: 200px;">    
                            </div>
                            <button type="button" class="navbar-toggle" data-toggle="offcanvas"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a href="http://www.sodhpuch.com/#" class="sp-nav-srh-trg visible-xs" data-toggle="modal" onclick="doko.nsftv(event, this);"><span class="glyphicon glyphicon-search"></span></a>
                            <a class="navbar-brand brand-nohome hide" href="http://www.sodhpuch.com/"><img class="sp-logo" src="images/sp-logo.png" alt="sp-logo">
                            </a>
                        </div>

                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-7 sp-nav-search">
                            <div class="sp-nav-search-xs hidden-xs" id="sp-__fgds">
                                <div class="row" style="position:relative;">
                                    <div class="xs-modal-search">
<?php echo form_open_multipart('organization/search_with_pagination', 'class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs"'); ?>  
                                        <form class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs" role="search" id="__fgds" method="get" action="http://www.sodhpuch.com/search/" onsubmit="return doko.__fgsps();">
                                            <div class="form-group col-xs-12 col-sm-6" id="search-q"> 
                                                <i class="fa fa-search"></i> 
<?php
echo form_input(array(
    'name' => 'organization',
    'value' => '',
    'placeholder' => 'Eg:Khojam Pvt. Ltd.',
    'class' => 'col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input',
));
?>
                                                <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" data-toggle="popover" autocomplete="off" value="" class="col-xs-12 form-control input-lg sp-input-nav-search tt-hint" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" data-toggle="popover" name="q" autocomplete="off" value="" id="q" class="col-xs-12 form-control input-lg sp-input-nav-search tt-input" placeholder="Search Keywords" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-q"></div></span></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-5" id="search-lt"> 
                                                <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-hint" autocomplete="off" value="" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input" autocomplete="off" name="_lt" id="_lt" value="" placeholder="Location" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-_lt"></div></span></span> <i class="fa fa-map-marker"></i>
                                            
<?php
echo form_input(array(
    'name' => 'location',
    'value' => '',
    'placeholder' => 'Location',
    'class' => 'col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input',
));
?>
                                                <i class="fa fa-map-marker"></i>
                                            </div>
                                                                       
                                            <div class="form-group col-xs-12 col-sm-1">
                                                <input type="hidden" name="_qt" id="_qt" maxlength="1" value="A">
                                                <input type="hidden" name="_rt" id="_rt" maxlength="1" value="">
                                                <input type="hidden" name="l" id="l" maxlength="100" value="">
<?php
echo form_submit(array(
    'name' => 'submit',
    'value' => 'Submit',
    'class' => 'btn btn-lg sp-btn-nav-search sp-btn-go',
    'id' => 'btn1',
));
?><span class="glyphicon glyphicon-search hidden-xs"></span><span class="visible-xs">Search</span>
                                                <button type="submit" class="btn btn-lg sp-btn-nav-search sp-btn-go"><span class="glyphicon glyphicon-search hidden-xs"></span><span class="visible-xs">Search</span>
                                                </button>
                                            </div>
<?php echo form_close(); ?>
                                            </form>
                                        <form class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs" role="search" id="__fgds" method="get" action="http://www.sodhpuch.com/search/" onsubmit="return doko.__fgsps();">
                                            <div class="form-group col-xs-12 col-sm-6" id="search-q"> 
                                                <i class="fa fa-search"></i> 
                                                <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" data-toggle="popover" autocomplete="off" value="" class="col-xs-12 form-control input-lg sp-input-nav-search tt-hint" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" data-toggle="popover" name="q" autocomplete="off" value="" id="q" class="col-xs-12 form-control input-lg sp-input-nav-search tt-input" placeholder="Search Keywords" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-q"></div></span></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-5" id="search-lt"> <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-hint" autocomplete="off" value="" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input" autocomplete="off" name="_lt" id="_lt" value="" placeholder="Location" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-_lt"></div></span></span> <i class="fa fa-map-marker"></i> </div>
                                            <div class="form-group col-xs-12 col-sm-1">
                                                <input type="hidden" name="_qt" id="_qt" maxlength="1" value="A">
                                                <input type="hidden" name="_rt" id="_rt" maxlength="1" value="">
                                                <input type="hidden" name="l" id="l" maxlength="100" value="">
                                                <button type="submit" class="btn btn-lg sp-btn-nav-search sp-btn-go"><span class="glyphicon glyphicon-search hidden-xs"></span><span class="visible-xs">Search</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-3">
                            <div class="col-sm-5" style="padding:0px;">
                                <ul class="list-inline nav navbar-nav navbar-left sp-navbar-main hidden-xs">
                                    <li><a href="#"><i class="fa fa-home" style="color:#fff; font-size: 19px;"></i></a>
                                    </li>
                                    <li><a href="#"> <i class="fa fa-bars" style="color:#fff; font-size: 19px;"></i> </a>
                                </ul>
                            </div>
                            <div class="col-sm-7">
                                <div>
                                    <i class="fa fa-phone" style="color: #fff; padding-top: 10px;"> &nbsp;+97111111111</i>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    <i class="fa fa-envelope-o" style="color: #fff; padding-top: 5px;"> &nbsp;abc@gmail.com</i>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-offcanvas row" style="height:auto;">
                            <div class="container-fluid">
                                <p class="navbar-text navbar-right sp-tf tooltip-trg" data-original-title="Sodhpuch Toll Free Number&lt;br/&gt;This service is currently available between&lt;br/&gt;&lt;span class=&#39;tf-tth&#39;&gt;8:00 AM - 8:00 PM&lt;/span&gt;" data-toggle="tooltip" data-placement="bottom"><strong><i class="fa fa-phone-square sp-mrgR5"></i>16600 197 197</strong>
                                </p>

                                <ul class="nav navbar-nav navbar-right sp-navbar-main-right">
                                    <li class="visible-xs"><a href="#" title="About Sodhpuch Pvt. Ltd.">Home</a>
                                    </li>
                                    <li class="visible-xs"><a href="#" title="Explore Sodhpuch Discount Offers">About Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>-->
<!--        <div class="row row-offcanvas row-offcanvas-right">-->
<!--..notice-->
<div class="container" id="doko-error-box"> </div>
<!--..notice-->
<div class="sp-page sp-container-box"  style="background-color: #F2F6FA;">
    <div class="container" style="margin-top: 10px; margin-bottom: 0px;">
        <div class="contact_map" id="googleMap" style="width:100%;height:380px;" >
                <?php // <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.147570875195!2d85.3280858!3d27.7127296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb190eb9311945%3A0x5c655f5be23e17a9!2sTREK+HIMALAYAN+NEPAL+PVT.+LTD.!5e0!3m2!1sen!2s!4v1435903888246" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> ?>
        </div>
        <div id="khojam1" class="col-lg-12">
            <div class="col-lg-8" style="padding:0px; margin-top: 15px;">
                <div class="panel panel-danger">
                    <h3 style="padding-left:10px; margin-top: 10px; color: #0962A1;">Search result: <?php echo $start; ?> to <?php echo $end; ?> of total <?php echo $total; ?> adds</h3>
                </div>
<?php $c = 0; ?>
<?php
//                         var_dump($location_info);die('fg');
foreach ($location_info->result() as $row) {
    ?>
                    <div class="well">
                        <div class="media" style="clear: both; overflow: hidden;">
                            <div class="col-md-12" style="padding:0px;">
                                <div class="media-left" style="width: 28%; overflow: hidden;">
                                    <a href="<?php echo base_url() ?>organization/view_final/<?php echo $row->category_slug; ?>/<?php echo $row->district; ?>/<?php echo $row->slug; ?>">
                                        <?php
                                        $check_attachment2 = explode('.', $row->attachment2);
//       var_dump($check_attachment2);die; 
                                        if (isset($check_attachment2[1])) {
                                            ?>

                                            <img class="img-responsive" src="<?php echo base_url(); ?>uploads/organization/<?php echo $row->attachment2; ?>">
    <?php } else { ?>
                                            <img src="<?php echo base_url(); ?>design/frontend/img/logo.png" alt="" style="width: 100%;">
    <?php } ?>    

                                    </a>
                                </div>
                                <div class="media-body" style="overflow: hidden;">
                                    <a href="<?php echo base_url() ?>organization/view_final/<?php echo $row->category_slug; ?>/<?php echo $row->district; ?>/<?php echo $row->slug; ?>">
                                        <h2 class="media-heading"style="color:#daa500;"><?php echo $row->name; ?></h2></a>
                                        <div class="col-md-4" style="padding-left:0px;">
                                            <h5><span class="glyphicon glyphicon-map-marker"style="color:#D73D3D;"></span>&nbsp; <?php echo $row->map_location; ?></h5>

                                        </div>
                                        <div class="col-md-4" style="padding-left:0px;">
                                            <h5><span class="glyphicon glyphicon-envelope"  style="color:#FFA500;"></span>&nbsp; <?php echo $row->website; ?> </h5>
                                        </div>
                                        <div class="col-md-4" style="padding-left:0px;">
                                            <h5><span class="glyphicon glyphicon glyphicon-earphone" style="color:#40E0D0;"></span>&nbsp; <?php echo $row->phone_no; ?></h5>
                                        </div>
    <?php if (!isset(${'working_id_' . $c})) {
        ${'working_id_' . $c} = '';
    } ?>
                                        <h5><span class="glyphicon glyphicon-calendar"style="color:#D73D3D;"></span> <?php echo ${'working_id_' . $c}; ?> </h5>
                                        <p style="margin-bottom:0px;"><?php echo $row->description; ?></p>
                                        <input id="input-21e" value="<?php echo (($row->org_rating == '') ? 0 : $row->org_rating) ?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xs" >
                                        </div>
                                        </div>
                                        </div>              
                                        </div>

    <?php $c++;
} ?>

                                    <div id="pagination">
                                        <?php echo $page_links; ?>
                                    </div>
                            </div>


                            <div style="margin-top:15px;" class="col-lg-4">

                                <div class=" pull-right">
                                    <div style="top: 80px;" class="" >
                                        <!--<noscript data-reactid=".0.0.0.0"></noscript>-->
                                <?php
                                //                                 var_dump($advertisement);die;
                                            foreach ($advertisement as $row) { 
    ?>
                                            <div  class="">
                                                <a href="<?php echo $row['link']; ?>">
                                                    <!--<img class="" src="<?php echo base_url(); ?>uploads/banner/1.jpg" >-->
                                                    <img class="" src="<?php echo base_url(); ?>uploads/advertisement/<?php echo $row['attachment'];?>" >
                                                </a>
                                            </div>
<?php } ?>
                                        <!--                                <div data-reactid=".0.0.0.1:$AD1" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD1.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD1.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928740NR-Accordion-v2-Tile-APM-300x50.png"></a>
                                                                        </div>
                                                                        <div data-reactid=".0.0.0.1:$AD2" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD2.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD2.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928759NR-Accordion-v2-Tile-Insights-300x50.png"></a>
                                                                        </div><div data-reactid=".0.0.0.1:$AD3" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD3.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD3.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928765NR-Accordion-v2-Tile-Mobile300x50.png"></a>
                                                                        </div>
                                                                        <div data-reactid=".0.0.0.1:$AD4" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD4.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD4.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928753NR-Accordion-v2-Tile-Browser-300x50.png"></a>
                                                                        </div>
                                                                        <div data-reactid=".0.0.0.1:$AD5" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD5.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD5.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928721NR-Accordion-v2-Tile-Synthetics-300x50.png"></a>
                                                                        </div>
                                                                        <div data-reactid=".0.0.0.1:$AD6" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD6.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD6.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928775NR-Accordion-v2-Tile-Servers-300x50.png"></a>
                                                                        </div>
                                                                        <div data-reactid=".0.0.0.1:$AD7" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                                                            <a data-reactid=".0.0.0.1:$AD7.0" target="_blank" href="#">
                                                                                <img data-reactid=".0.0.0.1:$AD7.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928770NR-Accordion-v2-Tile-Plugins-300x50.png"></a>
                                        -->                                </div>
                                    <!--<noscript data-reactid=".0.0.0.2"></noscript></div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--</div>-->
                <!-- footer wrapper -->
                <!-- Begin footer -->

                <!--                <div id="__doko-loading">Processing, please wait...</div>
                                <div class="hidden" id="ab1-popover-tl"><span class="text-danger">Login Required!</span>
                                </div>
                                <div class="hidden" id="ab1-popover-bd"><small><p class="mrgD5">Please <a href="http://www.sodhpuch.com/login/?r=http%3A%2F%2Fwww.sodhpuch.com%2Fnepal-bank-ltd."><strong>login</strong></a> to perform this activity</p><p class="mrgD0"><strong>New to Sodhpuch?</strong></p><p><a href="http://www.sodhpuch.com/signup">Sign Up</a> | <a href="http://www.sodhpuch.com/register-company">Register Your Business</a>.</p></small>
                                </div>
                                 Modal 
                                <div class="modal fade" id="doko-modal-box" tabindex="-1" role="dialog" aria-labelledby="doko-modal-title" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">ï¿½</button>
                                                <h4 class="modal-title" id="doko-modal-title">Modal title</h4> </div>
                                            <div class="modal-body" id="doko-modal-body"></div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" id="doko-modal-submit" onclick="javascript:void(0);" data-loading-text="Processing...">Save changes</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                         /.modal-content 
                                    </div>
                                     /.modal-dialog 
                                </div>
                                 /.modal 
                                 End footer 
                                 ..footer wrapper 
                            </div>
                             JS Goodies 
                -->
                <!--            
                </body>
                </html>-->
                <link href="<?php echo base_url(); ?>design/frontend/css/star-rating.css" rel="stylesheet" />
                <script src="<?php echo base_url(); ?>design/frontend/js/star-rating.js" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $("body").on("click", "#pagination a", function () {
                            var theUrl = $(this).attr('href');
                            $("#khojam1").load(theUrl);
                            return true;
                        });
                    });
                </script>

