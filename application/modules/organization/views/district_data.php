
<script
src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script>

info_win={
	      show:false,
	      content:"",
	      marker:new google.maps.Marker()
};
distance=[];
 infowindows = new google.maps.InfoWindow();
 path = new google.maps.MVCArray();
function initialize() {
	var map=new google.maps.Map(document.getElementById("googleMap"),{zoom:14});
	  var infowindow = new google.maps.InfoWindow();
	  var bounds = new google.maps.LatLngBounds();
	if (navigator.geolocation) {
	    navigator.geolocation.getCurrentPosition(function(position) {
	      var pos = {
	        lat: position.coords.latitude,
	        lng: position.coords.longitude
	      };
		 	bounds.extend(new google.maps.LatLng(pos.lat,pos.lng));
// 	      infoWindow.setPosition(pos);
// 	      infoWindow.setContent('Location found.');
// 	      infoWindow.open(map);

	    var currPosMarker=new google.maps.Marker({animation:google.maps.Animation.BOUNCE});
			currPosMarker.setPosition(pos);
			currPosMarker.setMap(map);
			currPosMarker.addListener( 'mouseover', function() {
      				infowindow.setContent('<b>You are here!<b>');
      			  infowindow.open(map,currPosMarker);
      			  });
			currPosMarker.addListener( 'mouseout', function() {
      			  infowindow.close(map);
      			  });	
	      map.setCenter(pos);
	      path.push(pos);
	      count= 0;
	      var marker;
	     
	      
	      <?php $c1=0; foreach($location_info->result() as $row)
	      {?>
	      
	      //***************************DISTANCE STUFFS***********************************//
			var origin = new google.maps.LatLng(pos.lat, pos.lng);
			var destination = new google.maps.LatLng(<?php echo $row->latitude;?>,<?php echo $row->longitude;?>);
			 getDestinationInfo(map,origin,destination,count,info_win);
			// alert( distanceInfo.distance);
			//*************************************************************//
	   		
	   		
	      	 marker=new google.maps.Marker({
	      		position:new google.maps.LatLng(<?php echo $row->latitude;?>,<?php echo $row->longitude;?>),
	      	});
	      		marker.setMap(map);
	      		
	      		google.maps.event.addListener(marker, 'click', (function(marker) {
	      			
		      		
	                return function() {
	                	var directionsService = new google.maps.DirectionsService;
	                	  var directionsDisplay = new google.maps.DirectionsRenderer;
	                	  
	                	  directionsDisplay.setMap(map);

	                	 
	                	  calculateAndDisplayRoute(directionsService, directionsDisplay,origin, new google.maps.LatLng(<?php echo $row->latitude;?>,<?php echo $row->longitude;?>));
	                	 
	                	
                	    info_win.content='<a href="<?php echo base_url(). $row->slug; ?>"><?php echo $row->name; ?></a>';
		                info_win.show=true;
		                info_win.marker=marker;
                	    getDestinationInfo(map,origin,destination,<?php echo $c1;?>,info_win);
	                   
	                }
	              })(marker));  
	      		
// 	      		marker.addListener( 'mouseout', function() {
// 	      			  infowindows.close(map);
	      			 
// 	      			  });
	      		
	      		count++;
	      		 bounds.extend(marker.position);
	                map.fitBounds(bounds);
	      <?php $c1++; }?>
	      //map.fitBounds(latlngbounds);
	      //alert(count);
	    }, function() {
		    //GeoLocation Error!!!
	    	 map.setZoom(14);
			 map.setCenter(new google.maps.LatLng(27.698268, 85.325289));
	    });
	  } else {
	    // Browser doesn't support Geolocation
		  map.setZoom(14);
		  map.setCenter(new google.maps.LatLng(27.698268, 85.325289));
	  }
	
  
}
function getDestinationInfo(map,origin,destination,index,info_win)
{
	//***************************DISTANCE STUFFS***********************************//
	 if(!info_win.show){
		 service = new google.maps.DistanceMatrixService();
			
			service.getDistanceMatrix(
			    {
			        origins: [origin],
			        destinations: [destination],
			        travelMode: google.maps.TravelMode.DRIVING,
			        avoidTolls: true
			    }, 
			    callback
			);
			
			function callback(response, status) {
		         if(status=="OK") {
		        	
		        	
		        	// alert( this.distanceInfo.distance+index);
		        	
		        		 distance[index] = response.rows[0].elements[0].distance.text;
		            	 var currAddress=response.originAddresses[0];
		        		 var content = '<p>'+distance[index]+' from your location('+currAddress+')</p>';
		                 document.getElementById("dist_info"+index).innerHTML=content;
		                 
		                
		           
		            
		         } else {
		             alert("Error: " + status);
		         }
		     }
         
        
     }
	 else
	 {
    	 info_win.show=false;
		//alert(index);
		var content = info_win.content+'<br><p>'+distance[index]+' from your location</p>';
        infowindows.setContent(content);
        infowindows.open(map, info_win.marker);
       
	 }
	
	    
	//*************************************************************//
}
/* road showing function*/
function calculateAndDisplayRoute(directionsService, directionsDisplay,origin,destination) {
	  directionsService.route({
	    origin:origin,
	    destination:destination,
	    travelMode: google.maps.TravelMode.DRIVING
	  }, function(response, status) {
	    if (status === google.maps.DirectionsStatus.OK) {
	      directionsDisplay.setDirections(response);
	    } else {
	      window.alert('Directions request failed due to ' + status);
	    }
	  });
	}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php
// var_dump($day);die;
if($day!=''){
foreach($day as $key=>$value){
    ${'working_id_'.$key} = $value;
}
}

?>
            <div class="sp-page sp-container-box"  style="background-color: #F2F6FA;">
                    <div class="container">
                        <div class="contact_map" id="googleMap" style="width:100%;height:380px;" >
                       <?php // <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.147570875195!2d85.3280858!3d27.7127296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb190eb9311945%3A0x5c655f5be23e17a9!2sTREK+HIMALAYAN+NEPAL+PVT.+LTD.!5e0!3m2!1sen!2s!4v1435903888246" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>?>
                    </div>
                    <div class="col-lg-8" style="padding:0px; margin-top: 15px;">
<!--                        <div class="panel panel-danger">
                            <h3 style="padding-left:10px; margin-top: 10px; color: #0962A1;">Search result: 1 to 20 of total 4330 adds</h3>
                        </div>-->
                    <?php $total_data= count($location_info);?>
                        <div class="panel panel-danger">
                            <h3 style="padding-left:10px; margin-top: 10px; color: #0962A1;">Search result: <?php echo $start; ?> to <?php echo $end; ?> of total <?php echo $total; ?> adds</h3>
                        </div>
                         
                            <?php $c=0;?>
                                 <?php
//                             var_dump($location_info->result());die('dfs');
                                
                                 foreach ($location_info->result_array() as $row) { ?>                           
                        <div class="well" style="padding:10px !important;">
                            <div class="media">
                                  <div class="media-left" style="width: 28%;">
                              <a href="<?php echo base_url()?>organization/view_final/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>/<?php echo $row['slug']; ?>">
                                  <?php $check_attachment2 = explode('.', $row['attachment2']);
//       var_dump($check_attachment2);die; 
                                  if(isset($check_attachment2[1])){?>
                                
                              <img class="img-responsive" src="<?php echo base_url();?>uploads/organization/<?php echo $row['attachment2']; ?>" style="margin-top:3px;">
                                  <?php }else{?>
                               <img src="<?php echo base_url();?>design/frontend/img/logo.png" alt="" style="width: 100%;">
                                  <?php }?>
                                </a>
                            </div>
                            <div class="media-body">
                                  <a href="<?php echo base_url()?>organization/view_final/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>/<?php echo $row['slug']; ?>">
                                <h2 class="media-heading"style="color:#daa500;"><?php echo $row['name'];?></h2>
                                  </a>
                                <div class="col-md-4" style="padding-left:0px;">
                                    <h5><span class="glyphicon glyphicon-envelope" style="color:#FFA500;"></span>&nbsp; <?php echo $row['website'];?> </h5>
                                </div>
                                <div class="col-md-4" style="padding-left:0px;">
                                    <h5><span class="glyphicon glyphicon glyphicon-earphone" style="color:#40E0D0;"></span>&nbsp; <?php echo $row['phone_no'];?></h5>
                                </div>
                                <div class="col-md-4" style="padding-left:0px;">
                                    <h5><span class="" style="color:#40E0D0;">Established</span>&nbsp; <?php echo $row['establish'];?></h5>
                                </div>
                                <?php if(!isset(${'working_id_'.$c})){${'working_id_'.$c}='';}?>
                                <h5><span class="glyphicon glyphicon-calendar"style="color:#D73D3D;"></span><?php echo ${'working_id_'.$c};?> </h5>
                                <div class="col-md-4" style="padding-left:0px;">
                                    <h5><span class="" style="color:#40E0D0;">Description</span>&nbsp; <?php echo $row['description'];?></h5>
                                </div>
                                <div class="col-md-4" style="padding-left:0px;">
                                    <h5><span class="glyphicon glyphicon-map-marker"style="color:#D73D3D;"></span>&nbsp; <?php echo $row['map_location'];?></h5>
                                    <p id="dist_info<?php echo $c;?>"></p>
                                </div>
                               <input id="input-21e" value="<?php echo (($row['org_rating']=='')?0:$row['org_rating'])?>" type="number" class="rating" min=0 max=5 step=0.5 data-size="xs" >
                               <div class="overall-rating">
                                    (Average Rating <span id="avgrat"><?php echo (int)$row['org_rating']; ?></span> Based on <span id="totalrat"><?php echo $row['count_rating']; ?></span>  rating)
                                </div>
                            </div>
                                 
                          </div>
                        </div>
                         <?php $c++; }?>
                        <div id="pagination">
                        <?php echo $page_links; ?>
                        </div>
                        </div>
                         <div style="margin-top:15px;" class="col-lg-4">
                    
                        <div class="advertisement pull-right">
                            <div style="top: 80px;" data-reactid=".0.0.0" class="AccordionInner u-fixed" id="undefined_inner">
                                <noscript data-reactid=".0.0.0.0"></noscript>
                                 <?php
//                             var_dump($advertisement->result());die('dfs');
                                
                                 foreach ($advertisement->result_array() as $row) { ?> 
                                
                                
                               <a href="<?php echo $row['link']; ?>">
                                        <img data-reactid=".0.0.0.1:$AD0.0.0" src="<?php echo base_url();?>uploads/advertisement/<?php echo $row['attachment']; ?>">
                                    </a>
                                 <?php }?>
<!--                                <div data-reactid=".0.0.0.1:$AD1" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD1.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD1.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928740NR-Accordion-v2-Tile-APM-300x50.png"></a>
                                </div>
                                <div data-reactid=".0.0.0.1:$AD2" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD2.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD2.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928759NR-Accordion-v2-Tile-Insights-300x50.png"></a>
                                </div><div data-reactid=".0.0.0.1:$AD3" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD3.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD3.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928765NR-Accordion-v2-Tile-Mobile300x50.png"></a>
                                </div>
                                <div data-reactid=".0.0.0.1:$AD4" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD4.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD4.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928753NR-Accordion-v2-Tile-Browser-300x50.png"></a>
                                </div>
                                <div data-reactid=".0.0.0.1:$AD5" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD5.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD5.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928721NR-Accordion-v2-Tile-Synthetics-300x50.png"></a>
                                </div>
                                <div data-reactid=".0.0.0.1:$AD6" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD6.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD6.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928775NR-Accordion-v2-Tile-Servers-300x50.png"></a>
                                </div>
                                <div data-reactid=".0.0.0.1:$AD7" style="height:50px;overflow:hidden;" class="Accordion_ad">
                                    <a data-reactid=".0.0.0.1:$AD7.0" target="_blank" href="#">
                                        <img data-reactid=".0.0.0.1:$AD7.0.0" src="http://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436928770NR-Accordion-v2-Tile-Plugins-300x50.png"></a>
                                </div>-->
                                <noscript data-reactid=".0.0.0.2"></noscript>
                               
                            </div>
                        </div>
                    </div>
                    
      
                </div>
            </div>           

<link href="<?php echo base_url();?>design/frontend/css/star-rating.css" rel="stylesheet" />
<script src="<?php echo base_url();?>design/frontend/js/star-rating.js" type="text/javascript"></script>


<!--<script>
    $(document).ready(function(){
      $("#khojam1").on("click", "#pagination a", function(){
          var theUrl = $(this).attr('href'); 
        $("#khojam1").load(theUrl);
       return true;
    });
    });
   </script>    -->