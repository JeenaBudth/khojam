<link href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
<script type="text/javascript" src="<?php echo base_url();?>design/admin/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script> 
                <link href="<?php echo base_url(); ?>design/frontend/css/rating.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/rating.js"></script>
<script language="javascript" type="text/javascript">
    <?php
 $ratingRow['average_rating']=3;
  $ratingRow['rating_number']=300;
?>
$(function() {
    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'postID'
    });
});

function processRating(val, attrVal){
alert(val);
    $.ajax({
        type: 'POST',
        url: <?php base_url()?>'rate/rating',
        data: 'postID='+attrVal+'&ratingPoints='+val,
        dataType: 'json',
        success : function(data) {
            if (data.status == 'ok') {
//                alert('You have rated '+val+' to CodexWorld');
                $('#avgrat').text(data.average_rating);
                $('#totalrat').text(data.rating_number);
            
            }else{
                alert('Some problem occured, please try again.');
            }
        }
    });
}
</script>
<style type="text/css">
    .overall-rating{font-size: 14px;margin-top: 5px;color: #8e8d8d;}
</style>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rating and Review</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
        <div class="widget-header"> 
<label class="label" for="name">Your Name</label><br>
            <input type="text" name="name" class="input-xlarge"><br>
            <label class="label" for="email">Your E-mail</label><br>
            <input type="email" name="email" class="input-xlarge"><br>
            <label class="label" for="message">Enter a Message</label><br>
            <textarea name="message" class="input-xlarge"></textarea>

<input name="rating" value="0" id="rating_star" type="hidden" postID="1" />
    <div class="overall-rating">(Average Rating <span id="avgrat"><?php echo $ratingRow['average_rating']; ?></span>
Based on <span id="totalrat"><?php echo $ratingRow['rating_number']; ?></span>  rating)</span></div>


            	
            </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>

$(document).ready(function(e) {
	
    $('#myModal').modal();
	

});

</script>


