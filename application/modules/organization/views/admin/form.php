<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/tinyfck/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">


    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        plugins: "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,paste,directionality,fullscreen,noneditable,contextmenu",
        theme_advanced_buttons1_add_before: "save,newdocument,separator",
        theme_advanced_buttons1_add: "fontselect,fontsizeselect",
        theme_advanced_buttons2_add: "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor,liststyle",
        theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
        theme_advanced_buttons3_add_before: "tablecontrols,separator",
        theme_advanced_buttons3_add: "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        plugin_insertdate_dateFormat: "%Y-%m-%d",
        plugin_insertdate_timeFormat: "%H:%M:%S",
        extended_valid_elements: "hr[class|width|size|noshade]",
        file_browser_callback: "fileBrowserCallBack",
        paste_use_dialog: false,
        theme_advanced_resizing: true,
        theme_advanced_resize_horizontal: false,
        theme_advanced_link_targets: "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
        apply_source_formatting: true

    });



    function fileBrowserCallBack(field_name, url, type, win) {

        var connector = "<?php echo base_url(); ?>assets/tinyfck/filemanager/browser.html?Connector=connectors/php/connector.php";

        var enableAutoTypeSelection = true;



        var cType;

        tinyfck_field = field_name;

        tinyfck = win;



        switch (type) {

            case "image":

                cType = "Image";

                break;

            case "flash":

                cType = "Flash";

                break;

            case "file":

                cType = "File";

                break;

        }



        if (enableAutoTypeSelection && cType) {

            connector += "&Type=" + cType;

        }



        window.open(connector, "tinyfck", "modal,width=600,height=400");

    }

</script>
<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> Organization</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/organization/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Name <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('name', $name, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Phone_no <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('phone_no', $phone_no, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Email <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('email', $email, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Established On <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('establish', $establish, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">District <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        $options = $all_district;
                        echo form_dropdown('district_id', $options, $district_id);
                        ?>
                    </div> 
                </div>

                <div class="form-group"> 

                    <label class="col-md-2 control-label">Description</label> 

                    <div class="col-md-10">

<?php echo form_textarea(array('id' => 'elm1', 'name' => 'description', 'value' => $description, 'rows' => '15', 'cols' => '80', 'style' => 'width: 100%', 'class' => 'form-control')); ?>

                    </div> 

                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Category <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        $options = $all_category;
                        echo form_dropdown('category_id', $options, $category_id);
                        ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Working_days <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        if (!empty($update_id)) {
                            $selected = explode(',', $working_id);
                        } else {
                            $selected = '';
                        }
                        $options = $all_working;
                        echo form_multiselect('select_working[]', $options, $selected);
                        ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Working_hour <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('working_hour', $working_hour, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Proprietor_name <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('prop_name', $prop_name, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Proprietor_phone <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('prop_phone', $prop_phone, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Proprietor_email <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('prop_email', $prop_email, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Address <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('map_location', $map_location, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Services <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        if (!empty($update_id)) {
                            $selected = explode(',', $services);
                        } else {
                            $selected = '';
                        }
                        $options = $all_services;
                        echo form_multiselect('services[]', $options, $selected);
                        ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Fax <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('fax', $fax, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Website <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('website', $website, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Latitude <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('latitude', $latitude, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Longitude<span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('longitude', $longitude, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta_tag <span class="required">*</span></label> 
                    <div class="col-md-10"> 
<?php echo form_input('meta_tag', $meta_tag, 'class="form-control required"'); ?>
                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta_description <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_textarea('meta_description', $meta_description, 'class="form-control required"'); ?>

                    </div> 
                </div>
                <div class="form-group">
                    	<label class="col-md-2 control-label">Banner_image(1140*400)px <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php
                            if (!empty($update_id)) {

                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment1
                                );
                            } else {
                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment1,
                                    'class' => 'required'
                                );
                            }
                            ?>
                            <?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="banner_image" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/organization/<?php echo $attachment1;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                </div>
                
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Icon (196*170)px<span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php
                            if (!empty($update_id)) {

                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment2
                                );
                            } else {
                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment2,
                                    'class' => 'required'
                                );
                            }
                            ?>
                            <?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="banner_image" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/organization/<?php echo $attachment2;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                    </div>
                 <div class="form-group">
                    	<label class="col-md-2 control-label">Advertise Audio <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php
                            if (!empty($update_id)) {

                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment3
                                );
                            } else {
                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile[]',
                                    'value' => $attachment3,
                                    'class' => 'required'
                                );
                            }
                            ?>
                            <?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Audio file (mp3/wma/mp4)</p>
                            
                        </div>
                </div>
                <div class="form-actions"> 
                    <?php
                    echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                    if (!empty($update_id)) {
                        echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

<?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>