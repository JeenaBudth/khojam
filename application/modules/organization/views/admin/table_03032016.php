<div class="manage">
                    <?php 
                $group_id = $this->session->userdata['group_id'];
                if($group_id!=1)
                    {
                    $mystring = implode(" ",$permissions);
                    if ((strpos($mystring, 'a'.$module_id))==true) 
                    {?>
    <input type="button" value="Add Organization" id="create" onclick="location.href='<?php echo base_url()?>admin/organization/create';"/>
                     <?php
                     }
                    }
                     else
                     {?>
    <input type="button" value="Add Organization" id="create" onclick="location.href='<?php echo base_url()?>admin/organization/create';"/>
                    <?php
                     }
                     ?>

</div>
<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i>Organization </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th>  
                    <th data-class="expand">Phone_no</th>
                    <th data-class="expand">Email</th>
                    <th data-class="expand">District</th>
                    <!--<th data-class="expand">Description</th>-->
                    <th data-class="expand">Category</th>
                    <!--<th data-class="expand">Working_days</th>-->
                    <!--<th data-class="expand">Working_hours</th>-->
                    <th data-class="expand">Proprietor_name</th>
                    <!--<th data-class="expand">Proprietor_phone</th>-->
                    <!--<th data-class="expand">Proprietor_email</th>-->
                    <th data-class="expand">Map_location</th>
                    <th data-class="expand">Services</th>
                    <th data-class="expand">Fax</th>
                    <th data-class="expand">Website</th>
                    <!--<th data-class="expand">Latitude</th>-->
                    <!--<th data-class="expand">Longitude</th>-->
                    <!--<th data-class="expand">Meta_tag</th>-->
                    <!--<th data-class="expand">Meta_description</th>-->
                    <th data-class="expand">Banner_image</th>
                    <th data-class="expand">Icon</th> 
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
                
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->title;?></td> 
                    <td><?php echo $row->phone_no;?></td>
                    <td><?php echo $row->email;?></td>
                    <td><?php echo $row->district;?></td>  
                    <!--<td><?php echo $row->description;?></td>-->
                    <td><?php echo $row->category;?></td>
                    <!--<td><?php echo $row->working_id;?></td>-->
                    <!--<td><?php echo $row->working_hour;?></td>-->
                    <td><?php echo $row->prop_name;?></td>
                    <!--<td><?php echo $row->prop_phone;?></td>-->
                    <!--<td><?php echo $row->prop_email;?></td>-->
                    <td><?php echo $row->map_location;?></td>
                    <td><?php echo $row->services;?></td>
                    <td><?php echo $row->fax;?></td>
                    <td><?php echo $row->website;?></td>
                    <!--<td><?php echo $row->latitude;?></td>-->
                    <!--<td><?php echo $row->longitude;?></td>-->
                    <!--<td><?php echo $row->meta_tag;?></td>-->
                    <!--<td><?php echo $row->meta_description;?></td>-->
                    <td><img src="<?php echo base_url();?>uploads/organization/<?php echo $row->attachment1;?>" style="height:50px;"/></td> 
                    <td><img src="<?php echo base_url();?>uploads/organization/<?php echo $row->attachment2;?>" style="height:50px;"/></td> 
                    
                    <td class="edit">
                    	<?php if($row->slug != 'home'){?>
                            <a href="<?php echo base_url()?>admin/organization/view/<?php echo $row->id;?>"><?php if($group_id==1){echo '<i class="icon-eye-open"> / </i>';} elseif((strpos($mystring, 'v'.$module_id))==true){echo '<i class="icon-eye-open"> / </i>';} ?></a>                   
                            
                            <a href="<?php echo base_url()?>admin/organization/create/<?php echo $row->id;?>"><?php if($group_id==1){echo '<i class="icon-pencil"> / </i>';} elseif((strpos($mystring, 'e'.$module_id))==true){echo '<i class="icon-pencil"> / </i>';} ?></a>                   
                            
                            <a href="<?php echo base_url()?>admin/organization/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><?php if($group_id==1){echo '<i class="icon-trash"></i>';} elseif((strpos($mystring, 'd'.$module_id))==true){echo '<i class="icon-trash"></i>';} ?></a>
                        <?php } ?>
                  </td>
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->