<script> 
$('.alert-success').hide();
</script>
<!--<link href="<?php echo base_url(); ?>design/frontend/css/blueimp-gallery.min.css" type="text/css" rel="stylesheet">-->
<!--<link href="<?php echo base_url(); ?>design/frontend/css/jquery.fileupload-ui.min.css" type="text/css" rel="stylesheet">-->
<link href="<?php echo base_url(); ?>design/frontend/css/sp-AYUw139DBk.v1.1.2.min.css" type="text/css" rel="stylesheet">

<!--start of    gallery section -->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/jquery-1.9.1.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/gallery/jssor.slider-20.mini.js"></script>
<!--end of gallery section-->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
            function init_map() {
            var var_location = new google.maps.LatLng(<?php echo $info[0]['latitude']; ?>, <?php echo $info[0]['longitude']; ?>);
                    var var_mapoptions = {
                    center: var_location,
                            zoom: 14
                    };
                    var var_marker = new google.maps.Marker({
                    position: var_location,
                            map: var_map,
                            title:"Kathmandu"});
                    var var_map = new google.maps.Map(document.getElementById("map-container"),
                            var_mapoptions);
                    var_marker.setMap(var_map);
            }

    google.maps.event.addDomListener(window, 'load', init_map);</script>
<!--                end of map script-->
<!-- header-->
<script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-67028726-1']);
            _gaq.push(['_trackPageview']);
            _gaq.push(['_trackPageLoadTime']);
            (function() {
            var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
            })();</script>

<div class="container">
    <div class="profile-cover-main">
        <div class="profile-cover-main-inner">
            <div class="profile-cover-hfix">
                <div class="profile-cover-imgholder"> 
                    <img src="<?php echo base_url() ?>uploads/organization/<?php echo $info[0]['attachment1'] ?>" id="-sp-cover-image" class="img-responsive" alt="Norvic Hospital" style="width:100%; height:400px;"> 
                    <a class="btn btn-default btn-sm hide sp-abs sp-abs-rt10 " data-gallery="profile-cover" title="View cover image large" id="_sp-cover-zoom" href="#files/large/9216">
                        <span class="glyphicon glyphicon-fullscreen"></span></a> 
                </div>
                <div class="container profile-logo-container">
                    <div class="logo-holder">
                        <div class="profile-logo">
                            <a href="#files/large/9215" class="text-center" title="Norvic Hospital" data-gallery="profile-logo"> 
                                <img id="profile-logo" class="img-responsive " src="<?php echo base_url() ?>uploads/organization/<?php echo $info[0]['attachment2'] ?>" alt="">
                                <span class="sp-logo-q logo-image hide" id="profile-logo-q"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="profile-subnav-main">
        <div class="container">
            <div class="profile-subnav-inner">
                <div class="" id="sp-profile-subnav-collapse">
                    <div class="col-md-8">
                        <h3 style="padding-left:5px;  margin-top:10px; color:#0962A1;"><?php echo $info[0]['name']; ?></h3>
                    </div>
                    <div class="col-md-4">
                        <i class="fa fa-eye sp-mrgR5 text-muted sp-pointer tooltip-trg " title="Total Views" style="padding-top:10px;"></i><strong><?php echo $info[0]['view_count']; ?></strong> views
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sp-profile-row row row-first">
        <div class="col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="profile_and_services">
                            <!--                                start of profile info-->

                            <div class="col-md-6 panel panel-default sp-prof-about sp-about-summary-card sp-section" style="    min-height: 345px;">
                                <div class="panel-heading" style="background-color:#0962A1 !important; background-image: none !important; color:#fff !important;">
                                    <h3 style="margin: 5px 0;"> <span><i class="fa fa-map-marker text-muted sp-mrgR10" style="color:#fff !important;"></i>Primary Contact Info</span> </h3> </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 sp-section-box">
                                            <div class="sub-section last">
                                                <div class="row" id="-box-primary-address">
                                                    <div class="col-xs-12">
                                                        <div class="panel panel-default sp-no-radius panel-primary-addr">
                                                            <div class="panel-body primary-address">
                                                                <div class="col-xs-12 col-md-7 sp-col-address">
                                                                    <h4 class="">
                                                                        <strong style="color:#666;">
                                                                            <i class="fa fa-phone-square sp-mrgR5"></i>
                                                                            Toll Free: 16600137373</strong>
                                                                    </h4>
                                                                    <address> <br> 
                                                                        Toll Free: 16600137373<br> 
                                                                        <?php echo $info[0]['map_location']; ?>, Kathmandu<br> 
                                                                        Nepal<br><br>
                                                                        <i class="fa fa-phone sp-mrgR10"></i>:
                                                                        <span class="sp-mrgL10"><?php echo $info[0]['phone_no']; ?></span><br> 
                                                                        <i class="fa fa-fax sp-mrgR10"></i>:
                                                                        <span class="sp-mrgL10"> <?php echo $info[0]['phone_no']; ?></span><br> 
                                                                        <span class="glyphicon glyphicon-link sp-mrgR10"></span>:
                                                                        <span class="sp-mrgL10" id="_pfwww13O">
                                                                            <a href="#" target="_blank" class="lnk"><?php echo $info[0]['email']; ?></a>
                                                                        </span><br>
                                                                        <i class="fa fa-envelope-o sp-mrgR10"></i>:
                                                                        <span class="sp-mrgL10"> 
                                                                            <a href="#" class="lnk"><?php echo $info[0]['website']; ?></a>
                                                                        </span><br> 
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-md-6 panel panel-default " style="    min-height: 345px;">
                                                                            <div class="sub-section">
                                                                                <h3 style="    color: #B98B00;"> <span>Our Services</span> </h3>
                                                                                <div class="we-deal">
                                                                                    <?php
                                                                                    $services = explode(',', $info[0]['services']);
                                                                                    foreach ($services as $row) {
                                                                                        ?>
                                                                                        <div class="sp-tags"> <span class="badge"><?php echo $row; ?></span>&nbsp; 
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <!--                                    start of working days-->
                                                                            <div class="sub-section">
                                                                                <h3  style="    color: #B98B00;"> <span>Working Days</span> </h3>
                                                                                <div id="-sp-wh7z2g">
                                                                                    <style type="text/css">
                                                                                        .wh-green,
                                                                                        .working-hours-condensed > h4 > span.today.wh-green {
                                                                                            color: #329632
                                                                                        }
                                                                                    </style>
                                                                                    <div class="working-hours-condensed text-center" id="_whcQl8">
                                                                                        <!--                                                        <h4><span class="today">Today - Open Till 5:00 PM</span></h4>-->
                                                                                        <p class="text-center toggler"><a href="##" onclick="return doko.aMZk2KXQl8( & #39; #_whcQl8 & #39; , & #39; #_wheQl8 & #39; )" class="tooltip-trg" title="EXPAND ALL">EXPAND ALL<br><i class="fa fa-angle-down fa-lg"></i></a>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="working-hours-expanded hide" id="_wheQl8">
                                                                                        <!--                                                        <ul class="list-group working-hours-expanded">-->
                                                                                        <div class="sp-tags"> 
                                                                                            <span class="badge">Sunday</span>
                                                                                            <span class="badge">Monday</span>
                                                                                            <span class="badge">Tuesday</span> 
                                                                                            <span class="badge">Wednesday</span>
                                                                                            <span class="badge">Thursday</span>
                                                                                            <span class="badge">Friday</span>
                                                                                            <span class="badge">Saturday</span>
                                                                                        </div>
                                                                                        <p class="text-center toggler">
                                                                                            <a href="##" onclick="return doko.aMZk2KXQl8( & #39; #_whcQl8 & #39; , & #39; #_wheQl8 & #39; )" class="tooltip-trg" title="COLLAPSE ALL"><i class="fa fa-angle-up fa-lg"></i><br>COLLAPSE ALL</a>
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <!--                                    end of working days-->


                                                                            <!--                                    start of working hours-->
                                                                            <div class="sub-section">
                                                                                <h3  style="    color: #B98B00;"> <span>Working Hours</span> </h3>
                                                                                <div class="sp-tags"> 
                                                                                    <?php echo $info[0]['working_hour']; ?>

<!--                                                                                                    From:<span class="badge">2015-02-09</span>
To:<span class="badge">2015-02-09</span>-->
                                                                                </div>
                                                                            </div>

                                                                            <!--                                    end of working hours-->
                                                                        </div>

                                                                        <!-- end of profile info-->
                                                                        <div class="col-md-6">


                                                                        </div>

                                                                </div>
                                                                <div class="clearfix"></div>

                                                                <div class="panel panel-default sp-prof-about sp-section">
                                                                    <!-- Tabs -->
                                                                    <ul class="nav nav-tabs nav_tab_div" style="padding:0px;">
                                                                        <li class="active col-sm-6">
                                                                            <a id="1" href="#">Like and share</a>
                                                                        </li>
                                                                        <li class="col-sm-6" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><a id="2"  href="<?php echo base_url();?>organization/rate">Rating and Review</a></li>
                                                                         
                                                                        <!--                                      <li><a id="3" href="#">Tab 3</a></li>-->
                                                                    </ul>
<!--                                                                    <div class="confirm-div" id="confirm"></div>-->
                                                                    <div id="succes" style="display: none;">
                                                                    <div class="bs-example" >
                                                                        <div class=" alert-success fade in">
                                                                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                                            <strong>Success!</strong>  Successfully Rated.
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                    <!-- Accordions -->
                                                                    <div class="tabContent" id="tabContent1" style="overflow: hidden;">
                                                                        <img data-u="image" src="<?php echo base_url(); ?>design/frontend/images/facebook.png" />
                                                                    </div>
                                                                    <div class="tabContent" id="tabContent2" style="overflow: hidden;">
                                                                        <img data-u="image" src="<?php echo base_url(); ?>design/frontend/images/review_image.png" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-12 sidebar-panels">


                                                                <div id="map-container" class="thumbnail" style="margin-bottom:10px;"></div>
                                                                <!--  start of gallery section-->
                                                                <div class="row">
                                                                    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 456px; overflow: hidden; visibility: hidden; background-color: #24262e;">
                                                                        <!-- Loading Screen -->
                                                                        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                                                                            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                                                            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                                                        </div>
                                                                        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/01.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-01.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/02.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-02.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/03.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-03.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/04.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-04.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/05.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-05.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/06.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-06.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/07.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-07.jpg" />
                                                                            </div>
                                                                            <div style="display: none;">
                                                                                <img data-u="image" src="<?php echo base_url(); ?>design/frontend/img/08.jpg" />
                                                                                <img data-u="thumb" src="<?php echo base_url(); ?>design/frontend/img/thumb-08.jpg" />
                                                                            </div>
                                                                        </div>
                                                                        <!--                                                                                         <!-- Thumbnail Navigator -->
                                                                        <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;height:100px; width:100px;" data-autocenter="1">
                                                                            <!-- Thumbnail Item Skin Begin -->
                                                                            <div data-u="slides" style="cursor: default;">
                                                                                <div data-u="prototype" class="p">
                                                                                    <div class="w">
                                                                                        <div data-u="thumbnailtemplate" class="t"></div>
                                                                                    </div>
                                                                                    <div class="c"></div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Thumbnail Item Skin End -->
                                                                        </div>
                                                                        <!-- Arrow Navigator -->
                                                                        <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                                                        <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                                                                        <!--<a href="http://www.jssor.com" style="display:none">Jssor Slider</a>-->
                                                                    </div>
                                                                </div>


                                                                <!--end of gallery section -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                        <script>
                                                    jQuery(document).ready(function ($) {

                                            var jssor_1_SlideshowTransitions = [
                                            {$Duration:1200, x:0.3, $During:{$Left:[0.3, 0.7]}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x: - 0.3, $SlideOut:true, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x: - 0.3, $During:{$Left:[0.3, 0.7]}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, $SlideOut:true, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y:0.3, $During:{$Top:[0.3, 0.7]}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y: - 0.3, $SlideOut:true, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y: - 0.3, $During:{$Top:[0.3, 0.7]}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y:0.3, $SlideOut:true, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, $Cols:2, $During:{$Left:[0.3, 0.7]}, $ChessMode:{$Column:3}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, $Cols:2, $SlideOut:true, $ChessMode:{$Column:3}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y:0.3, $Rows:2, $During:{$Top:[0.3, 0.7]}, $ChessMode:{$Row:12}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y:0.3, $Rows:2, $SlideOut:true, $ChessMode:{$Row:12}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y:0.3, $Cols:2, $During:{$Top:[0.3, 0.7]}, $ChessMode:{$Column:12}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, y: - 0.3, $Cols:2, $SlideOut:true, $ChessMode:{$Column:12}, $Easing:{$Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, $Rows:2, $During:{$Left:[0.3, 0.7]}, $ChessMode:{$Row:3}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x: - 0.3, $Rows:2, $SlideOut:true, $ChessMode:{$Row:3}, $Easing:{$Left:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, y:0.3, $Cols:2, $Rows:2, $During:{$Left:[0.3, 0.7], $Top:[0.3, 0.7]}, $ChessMode:{$Column:3, $Row:12}, $Easing:{$Left:$Jease$.$InCubic, $Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, x:0.3, y:0.3, $Cols:2, $Rows:2, $During:{$Left:[0.3, 0.7], $Top:[0.3, 0.7]}, $SlideOut:true, $ChessMode:{$Column:3, $Row:12}, $Easing:{$Left:$Jease$.$InCubic, $Top:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, $Delay:20, $Clip:3, $Assembly:260, $Easing:{$Clip:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, $Delay:20, $Clip:3, $SlideOut:true, $Assembly:260, $Easing:{$Clip:$Jease$.$OutCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, $Delay:20, $Clip:12, $Assembly:260, $Easing:{$Clip:$Jease$.$InCubic, $Opacity:$Jease$.$Linear}, $Opacity:2},
                                            {$Duration:1200, $Delay:20, $Clip:12, $SlideOut:true, $Assembly:260, $Easing:{$Clip:$Jease$.$OutCubic, $Opacity:$Jease$.$Linear}, $Opacity:2}
                                            ];
                                                    var jssor_1_options = {
                                                    $AutoPlay: true,
                                                            $SlideshowOptions: {
                                                            $Class: $JssorSlideshowRunner$,
                                                                    $Transitions: jssor_1_SlideshowTransitions,
                                                                    $TransitionsOrder: 1
                                                            },
                                                            $ArrowNavigatorOptions: {
                                                            $Class: $JssorArrowNavigator$
                                                            },
                                                            $ThumbnailNavigatorOptions: {
                                                            $Class: $JssorThumbnailNavigator$,
                                                                    $Cols: 10,
                                                                    $SpacingX: 8,
                                                                    $SpacingY: 8,
                                                                    $Align: 360
                                                            }
                                                    };
                                                    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
                                                    //responsive code begin
                                                            //you can remove responsive code if you don't want the slider scales while window resizes
                                                                    function ScaleSlider() {
                                                                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                                                                            if (refSize) {
                                                                    refSize = Math.min(refSize, 800);
                                                                            jssor_1_slider.$ScaleWidth(refSize);
                                                                    }
                                                                    else {
                                                                    window.setTimeout(ScaleSlider, 30);
                                                                    }
                                                                    }
                                                            ScaleSlider();
                                                                    $(window).bind("load", ScaleSlider);
                                                                    $(window).bind("resize", ScaleSlider);
                                                                    $(window).bind("orientationchange", ScaleSlider);
                                                                    //responsive code end
                                                            });
                                        </script>

<script language="javascript" type="text/javascript">
 <?php
 $ratingRow['average_rating']=3;
 $ratingRow['rating_number']=300;
?>
$(function() {
    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'postID'
    });
});
$(document).ready(function() {
  $("#submit").click(function () {
      var base_url='<?php echo base_url()?>';
    var name= $("#name").val();
    var email=$('#email').val();
    var message=$('#message').val();
    var rating=$('#star').val();
    var org_id='<?php echo $this->uri->segment(5)?>';
    var district_id='<?php echo $this->uri->segment(4)?>';
    var cat_id='<?php echo $this->uri->segment(3)?>';
    
//alert(star);
       $.ajax({
        type: 'POST',
        url: base_url+'rate/rating',
        data: 'name='+name +'&message='+message + '&email=' + email+'&rating='+rating+'&org_id=' + org_id+'&district_id=' + district_id+'&cat_id=' + cat_id,
        dataType: 'json',
        success : function(data) {
            
            
            if (data == 'ok') {
                 $("#succes").show();
            $('#myModal').removeClass('show');

            
             
            }else{
                alert('Some problem occured, please try again.');
            }
        }
    });
  });
});
function processRating(val, attrVal){

$('#star').val(val);
}
</script>
<style type="text/css">
    .overall-rating{font-size: 14px;margin-top: 5px;color: #8e8d8d;}
</style>
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rating and Review</h4>
      </div>
       
      <div class="modal-body">
        <!--<p>Some text in the modal.</p>-->
        <div class="widget-header"> 
<input type="text" name="name" id="name" class="form-control" placeholder="Your Name"><br>
            <input type="email" name="email" id="email" class="form-control" placeholder="Your E-mail"><br>
            <textarea name="message" class="form-control" id="message" placeholder="Enter a Message"></textarea>
            <div class="clear-fix"></div>
<input name="rating" value="0" id="rating_star" type="hidden" postID="1" />
<input type="hidden" name="star" id="star" class="form-control" ><br>
    <div class="overall-rating">(Average Rating <span id="avgrat"><?php echo $ratingRow['average_rating']; ?></span>
Based on <span id="totalrat"><?php echo $ratingRow['rating_number']; ?></span>  rating)</span></div>
            </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="submit" data-dismiss="modal">Submit</button>
      </div>
    </div>

  </div>
</div>


