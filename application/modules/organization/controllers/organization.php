 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class organization extends MX_Controller

{

	function __construct() {

	parent::__construct();
        $this->load->library('pagination');
	}


        function index()
        {
             
		//$data['query'] = $this->get('id');
                $data['view_file']= 'front';                              
                $data['publications'] = $this->get_front_publication();
               
//                var_dump($data['photos']);die();
//                $this->load->view('front',$data);
		$this->load->module('template');
		$this->template->front($data);

	} 
        function get_organization(){ 
            $this->load->model('mdl_organization');
            if (isset($_GET['term'])){
                $q = strtolower($_GET['term']);
            $this->mdl_organization->get_organization($q);
        }
            }
            function get_location(){ 
            $this->load->model('mdl_organization');
            if (isset($_GET['term'])){
                $q = strtolower($_GET['term']);
            $this->mdl_organization->get_location($q);
        }
            }
        function search()
         {
            
            $this->session->set_userdata(array(
            'organization' => $_POST['organization'],
            'location' => $_POST['location'],
            ));

            $organization = $this->session->userdata('organization');
            $location = $this->session->userdata('location');
            $data['no_of_rows'] = $this->calculate_rows($organization, $location);
            if($data['no_of_rows']!=NULL){
            $data['num_links'] = 2;
            $config['per_page'] = 5;
            $config['use_page_numbers'] = TRUE;
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['total_rows'] = $data['no_of_rows']->num_rows();
            $config['base_url'] = base_url().'/organization/search_with_pagination';
            $this->pagination->initialize($config);
            $data['page_links'] = $this->pagination->create_links();
            $currpage = $this->uri->segment(3);

            if($currpage == ''){ 
            $currpage = 1;
            }
            $data['start']=(($config['per_page']* $currpage)-$config['per_page'])+1;
            $data['end']= $config['per_page']* $currpage; 
            $data['total']=$data['no_of_rows']->num_rows();
            $offset = ($currpage- 1) * $config['per_page'];
//            $data['info'] = $this->get_user_search($organization, $location,$config['per_page'], $offset);
            $data['location_info']=$this->mdl_organization->get_location_info_search($organization, $location,$config['per_page'], $offset);
//            var_dump($data['location_info']->result());die;
             $data['day']=$this->get_day($data['location_info']);
             $data['advertisement']=$this->get_search();
//             var_dump($data['advertisement']);die;
            $data['view_file'] = 'front_search';
            $this->load->module('template');
            $this->template->front($data);
            }
        else {
            redirect('home');
        }
       
        }
        function search_with_pagination()
        {
            $organization = $this->session->userdata('organization');
            $location = $this->session->userdata('location');
            $data['no_of_rows'] = $this->calculate_rows($organization, $location)->num_rows();
            $config['use_page_numbers'] = TRUE;
            $config['per_page'] = 5;
            $data['num_links'] = 2;
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';

            $config['total_rows'] = $data['no_of_rows'];
            $config['base_url'] = base_url().'/organization/search_with_pagination';
            $this->pagination->initialize($config);
            $data['page_links'] = $this->pagination->create_links();
            $currpage = $this->uri->segment(3);
            if($currpage == ''){ 
                $currpage = 1;
            }
            $data['start']=(($config['per_page']* $currpage)-$config['per_page'])+1;
            $data['end']= $config['per_page']* $currpage;      
            $data['total']=$data['no_of_rows'];
            $offset= ($currpage - 1) * $config['per_page'];
//            $data['info'] = $this->get_user_search($organization, $location,$config['per_page'], $offset);
            $data['location_info']=$this->mdl_organization->get_location_info_search($organization, $location,$config['per_page'], $offset);
            $data['day']=$this->get_day($data['location_info']);
             $data['advertisement']=$this->get_search();
            $data['view_file'] = 'front_search';
            $this->load->module('template');
            $this->template->front($data);

        }
          function details(){
            
            $this->load->model('mdl_organization');
            $slug = $this->uri->segment(3);
            $district = $this->uri->segment(4);
            if($district=='')
            {
                $query=$this->mdl_organization->get_id_from_slugs($slug);
                     
                $data['attachment2']=$query[0]['attachment2'];
//                var_dump( $data['attachment2']);die('ddd');
                $data['info']=$this->mdl_organization->get_district_name($query[0]['id']);
//                var_dump($data['info']);die('df');
                $data['advertisement']=$this->get_category($query[0]['id']);
//                var_dump($data['advertisement']->result());die('jeena');
                $data['view_file']="catagory_district";
            }
            else{
                
                $cat_id=$this->mdl_organization->get_id_from_slug($slug);
                $dis_id=$this->mdl_organization->get_id_district_slug($district);
                $data['no_of_rows']=$this->mdl_organization->get_org_name($cat_id,$dis_id);
                $config['use_page_numbers'] = TRUE;
                $config['per_page'] = 5;
                $config['num_links'] = 2;
                $config['next_link'] = 'Next';
                $config['prev_link'] = 'Prev';
                $config['uri_segment'] = 5;
                $config['total_rows'] = $data['no_of_rows'];
//                var_dump($config); die;
                $config['base_url'] = base_url().'/organization/details/'.$slug.'/'.$district;
                 $currpage = $this->uri->segment(5);
                $this->pagination->initialize($config);
                $data['page_links'] = $this->pagination->create_links();
//                var_dump($data['page_links']); die;
               
                if($currpage == ''){ 
                   $currpage = 1;
                }
                $data['start']=(($config['per_page']* $currpage)-$config['per_page'])+1;
                $data['end']= $config['per_page']* $currpage; 
                $data['total']=$data['no_of_rows'];
                $offset= ($currpage - 1) * $config['per_page'];
//                $data['info'] = $this->mdl_organization->get_org_name_limited($cat_id, $dis_id,$config['per_page'], $offset);
                $data['location_info']=$this->mdl_organization->get_location_info($cat_id, $dis_id,$config['per_page'], $offset);
//                var_dump($data['location_info']->result());die('df');
                /* for day conversion*/
                $data['day']=$this->get_day($data['location_info']);
                $data['advertisement']=$this->get_district($cat_id,$dis_id);
//                var_dump($data['advertisement']->result());die;
                $data['view_file']="district_data"; 
       
            }
              
            $this->load->module('template');
            $this->template->front($data);
            }
            
             function view_final()
            {
//                $data['photos'] = $this->get_photos('id');
                $this->load->model('mdl_organization');
                $slug_cat=$this->uri->segment(3);
                $cat_id=$this->mdl_organization->get_id_from_slug_cat($slug_cat);
                $slug_org=$this->uri->segment(5);
                $org_id=$this->mdl_organization->get_id_from_slug_org($slug_org);
                $district=$this->uri->segment(4);
                $dis_id=$this->mdl_organization->get_id_district_slug($district);
           
                
                $data['photos'] = $this->get_photos($org_id);

                $data['info']=$this->mdl_organization->get_view_details($cat_id,$org_id);
//                var_dump( $data['info']);die('ggg');
                $data['day']=$this->get_days($data['info']);
                $data['advertisement']=$this->get_organization_attachment($cat_id,$dis_id,$org_id);

                $data['latest_rate']=$this->get_rate($cat_id,$dis_id,$org_id);
              
                mysql_query("UPDATE up_organization SET view_count = view_count + 1 where id= $org_id");
               
//                var_dump($data['photos']);die;
                $data['view_file']="view_final";
                $this->load->module('template');
                $this->template->front($data);
                
            }
            function rate(){
                $this->load->view('rate');
            }
            
            function front(){
                $this->load->model('mdl_organization');
            $slug = $this->uri->segment(3);
            $id=$this->mdl_organization->get_id_from_slug($slug);
           $data['info']=$this->mdl_organization->get_org_name($id);
           
            $data['view_file']="front";
            $this->load->module('template');
            $this->template->front($data);
            }
                    
            
            
        function get_publication_details($slug){

	$this->load->model('mdl_publication');

	$query = $this->mdl_publication->get_publication_details($slug);

	return $query->result_array();

	} 
            
        function get_details_from_slug($slug){

	$this->load->model('mdl_publication');

	$query = $this->mdl_publication->get_details_from_slug($slug);

	return $query->result_array();

	}
        function get_user_search($organization, $location, $limit,$offset){
            $this->load->model('mdl_organization');
            $query = $this->mdl_organization->get_user_search($organization, $location, $limit,$offset);
            return $query;
        }
        function calculate_rows($organization, $location){
            $this->load->model('mdl_organization');
            $query = $this->mdl_organization->calculate_rows($organization, $location);
            return $query;
        }
          function get_location_info($id){
            $this->load->model('mdl_organization');
            $query = $this->mdl_organization->get_location_info($id);
            return $query;
        }
        function get_front_publication(){	
		$this->load->model('mdl_publication');
		$query = $this->mdl_publication->get_front_publication();
		$result = $query->result_array();
		return $result;
	}
         function get_photos($org_id)
        {	
//            echo$org_id;die;
		$this->load->model('photos/mdl_photos'); 
		$query = $this->mdl_photos->get_final_photos($org_id);
		$result = $query->result_array();
//                var_dump($result);die('vb');
		return $result;
	}
          function get_rate($cat_id,$dis_id,$org_id)
        {	
//            echo$org_id;die;
		$this->load->model('rate/mdl_rating'); 
		$query = $this->mdl_rating->get_rate($cat_id,$dis_id,$org_id);
		
//                var_dump($query->result());die('vb');
		return $query;
	}

        function get($order_by){

	$this->load->model('mdl_news_update');

	$query = $this->mdl_news_update->get($order_by);

	return $query;

	}

       
	function get_day($data){
            foreach($data->result()as $keyy=>$value_arr)
                {
                
                foreach($value_arr as $key=>$value)
                {
                ${$key.'_'.$keyy} = $value;
                }

            }
//            var_dump($data->result());die;
          foreach($data->result()as $keyy=>$value_arr){
              if(${'working_id_'.$keyy}!=null){
           $days = explode(',',${'working_id_'.$keyy});
           $this->load->model('working/mdl_working');
           $day[$keyy] = $this->mdl_working->get_equivalent_day($days);
              }
           }
//           var_dump($day);die;
           if(isset($day))
           {
           return $day;
           }
            else {
                return '';
            }

           
        }
        
        function get_days($data){
            foreach($data as $keyy=>$value_arr)
                {
                foreach($value_arr as $key=>$value)
                {
                ${$key.'_'.$keyy} = $value;
                }

            }
//            var_dump($data);die;
          foreach($data as $keyy=>$value_arr){
              if(${'working_id_'.$keyy}!=null){
           $days = explode(',',${'working_id_'.$keyy});
           $this->load->model('working/mdl_working');
           $day[$keyy] = $this->mdl_working->get_equivalent_day($days);
              }
           }
//           var_dump($day);die;
           if(isset($day))
           {
           return $day;
           }
            else {
                return '';
            }

           
        }
        
        function get_search()
        {
            
            $this->load->model('advertisement/mdl_advertisement'); 
            $query = $this->mdl_advertisement->get_search();
            $result = $query->result_array();
            //                var_dump($result);die('vb');
            return $result;
	}
        
        function get_category($cat_id)
        {
            $this->load->model('advertisement/mdl_advertisement');
            $query = $this->mdl_advertisement->get_category($cat_id);
            return $query;
            
        }
        
          function get_district($cat_id,$dis_id)
        {
            $this->load->model('advertisement/mdl_advertisement');
            $query = $this->mdl_advertisement->get_district($cat_id,$dis_id);
            return $query;
            
        }
        
          function get_organization_attachment($cat_id,$dis_id,$org_id)
        {
            $this->load->model('advertisement/mdl_advertisement');
            $query = $this->mdl_advertisement->get_organization($cat_id,$dis_id,$org_id);
            return $query;
            
        }
          function get_organization_ajax()
        {
              $dis_id=$_POST['district'];
              $cat_id=$_POST['category'];
            $this->load->model('mdl_organization');
            $query = $this->mdl_organization->get_organization_ajax($cat_id,$dis_id);
//            var_dump($query->result());die;
            echo json_encode($query->result());
            exit();
            
        }
        




	

	

}