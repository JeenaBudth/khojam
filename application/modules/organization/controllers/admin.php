<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module('admin_login/admin_login');
        $this->admin_login->check_session_and_permission('organization'); //module name is organization here	
    }

    function index() {
       
        $modulename = $this->uri->segment(2);
        $data['module_id'] = $this->get_id_from_modulename($modulename);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $data['permissions'] = $this->unserialize_role_array($group_id);
        $data['query'] = $this->get('id');
        $data['view_file'] = "admin/table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function get_data_from_post() {
        
        $data['name'] = $this->input->post('name', TRUE);
        $data['phone_no'] = $this->input->post('phone_no', TRUE);
        $data['email'] = $this->input->post('email', TRUE);
        $data['establish'] = $this->input->post('establish', TRUE);
        $data['district_id'] = $this->input->post('district_id', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['category_id'] = $this->input->post('category_id', TRUE);
        $data['working_hour'] = $this->input->post('working_hour', TRUE);
        $working_id=  $this->input->post('select_working',TRUE);
        if($working_id!='')
        {
        $data['working_id']=implode(',',$working_id);
        }
        $data['prop_name']=$this->input->post('prop_name',TRUE);
        $data['prop_phone']=$this->input->post('prop_phone',TRUE);
        $data['prop_email']=$this->input->post('prop_email',TRUE);
        $data['map_location']=$this->input->post('map_location',TRUE);
        $services=$this->input->post('services',TRUE);
        if($services!='')
        {
           $data['services']= implode(',',$services);
        }
       
        $data['fax']=$this->input->post('fax',TRUE);
        $data['website']=$this->input->post('website',TRUE);
        $data['latitude']=$this->input->post('latitude',TRUE);
        $data['longitude']=$this->input->post('longitude',TRUE);
        $data['meta_tag']=$this->input->post('meta_tag',TRUE);
        $data['meta_description']=$this->input->post('meta_description',TRUE);
        
        $data['slug'] = strtolower(url_title($data['name']));

        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $data['upd_date'] = date("Y-m-d");
            $attach = $this->get_attachment_from_db($update_id);
        } else {
            $data['ent_date'] = date("Y-m-d");
            $data['upd_date'] = NULL;
           $data['attachment1']=$this->input->post('userfile',TRUE);
        $data['attachment2']=$this->input->post('userfile',TRUE);
        $data['attachment3']=$this->input->post('userfile',TRUE);
        }
        return $data;
    }

    function get_data_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
            $data['phone_no'] = $row->phone_no;
            $data['email'] = $row->email;
            $data['establish'] = $row->establish;
            $data['district_id'] = $row->district_id;
            $data['description'] = $row->description;
            $data['category_id'] = $row->category_id;
            $data['working_id'] = $row->working_id;
            $data['working_hour'] = $row->working_hour;
            $data['prop_name']=$row->prop_name;
            $data['prop_phone']=$row->prop_phone;
            $data['prop_email']=$row->prop_email;
            $data['map_location']=$row->map_location;
            $data['services']=$row->services;
            $data['fax']=$row->fax;;
            $data['website']=$row->website;
            $data['latitude']=$row->latitude;
            $data['longitude']=$row->longitude;
            $data['meta_tag']=$row->meta_tag;
            $data['meta_description']=$row->meta_description;
            $data['attachment1']=$row->attachment1;
            $data['attachment2']=$row->attachment2;
            $data['attachment3']=$row->attachment3;
            
        }

        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }

    function get_data_for_view($update_id) {
        $query = $this->get_view($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
            $data['phone_no'] = $row->phone_no;
            $data['email'] = $row->email;
            $data['establish'] = $row->establish;
            $data['district_id'] = $row->district_id;
            $data['description'] = $row->description;
            $data['category_id'] = $row->category_id;
            $data['working_id'] = $row->working_days;
            $data['working_hour'] = $row->working_hour;
            $data['prop_name']=$row->prop_name;
            $data['prop_phone']=$row->prop_phone;
            $data['prop_email']=$row->prop_email;
            $data['map_location']=$row->map_location;
            $data['services']=$row->services;
             $data['fax']=$row->fax;;
            $data['website']=$row->website;
            $data['latitude']=$row->latitude;
            $data['longitude']=$row->longitude;
            $data['meta_tag']=$row->meta_tag;
            $data['meta_description']=$row->meta_description;
            $data['attachment1']=$row->attachment1;
            $data['attachment2']=$row->attachment2;
            $data['attachment3']=$row->attachment3;



            if (!isset($data)) {
                $data = "";
            }
            return $data;
        }
    }
        function get_banner_image_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['banner_image'] = $row->banner_image;				
		}
			return $data;
	}
        function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment1'] = $row->attachment1;	
                        $data['attachment2'] = $row->attachment2;
                        $data['attachment3'] = $row->attachment3;
		}
//                echo '<pre>',  print_r($data),'</pre>';die;
			return $data;
	}
           

    function create() {
      
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $update_id = $this->uri->segment(4);

        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            //person has submitted the form
            $data = $this->get_data_from_post();
        }
        if (is_numeric($update_id)) {
       
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'e' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->get_data_from_db($update_id);
                }
            } else {
                $data = $this->get_data_from_db($update_id);
            }
        } else {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'a' . $module_id)) == false) {
                    redirect('admin/dashboard');
                }
            }
            //$data = $this->get_data_from_db($update_id);
        }


        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
        $all_working=$this->get_working();
        
        $all_services=$this->get_services();
        $all_category = $this->get_category();
        $all_district = $this->get_district();
        $data['all_services']=$all_services;
        $data['all_working']=$all_working;
//        var_dump($data['all_working']);die('jena');
        $data['all_category'] = $all_category;
        $data['all_district'] = $all_district;
        $data['update_id'] = $update_id;
        $data['view_file'] = "admin/form";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function delete() {
        $this->load->model('mdl_organization');
        $delete_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/organization');
        } else {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'd' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $this->mdl_organization->_delete($delete_id);
                    redirect('admin/organization');
                }
            }
            $this->mdl_organization->_delete($delete_id);
            redirect('admin/organization');
        }
    }

    function view() {
       
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $view_id = $this->uri->segment(4);
        if ($group_id != 1) {
            if (is_numeric($view_id)) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);

                if ((strpos($mystring, 'v' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->get_data_from_db($view_id);
                }
            }
        }
        $data = $this->get_data_from_db($view_id);
        $data['view_id'] = $view_id;
        $data['view_file'] = "admin/view";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {

        $this->load->library('form_validation');
        /* setting validation rule */
        $update_id = $this->input->post('update_id', TRUE);
        if (is_numeric($update_id)) {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
            $this->form_validation->set_rules('phone_no', 'Phone_no', 'required|xss_clean'); //we don't want unique_validation error while editing
            $this->form_validation->set_rules('email', 'Email', 'required|xss_clean'); //we don't want unique_validation error while editing
        } else {
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
            $this->form_validation->set_rules('phone_no', 'Phone_no', 'required|xss_clean'); //unique_validation check while creating new
            $this->form_validation->set_rules('email', 'Email', 'required|xss_clean'); //unique_validation check while creating new
        }
        /* end of validation rule */


        if ($this->form_validation->run($this) == FALSE) {
            $this->create();
        } else {
            $data = $this->get_data_from_post();

            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $attach = $this->get_attachment_from_db($update_id);
                
                $uploadattachment = $this->do_upload($update_id, $data);
                $data['attachment1'] = $uploadattachment['1']['upload_data']['file_name'];
                $data['attachment2'] = $uploadattachment['2']['upload_data']['file_name'];
                $data['attachment3'] = $uploadattachment['3']['upload_data']['file_name'];
                $result1=explode(".",$data['attachment1']);
                $result2=explode(".",$data['attachment2']);
                $result3=explode(".",$data['attachment3']);
                if(sizeof($result1)==1)
                {
                    $data['attachment1'] = $attach['attachment1'];
                }
                if(sizeof($result2)==1)
                {
                    $data['attachment2'] = $attach['attachment2'];
                }
                if(sizeof($result3)==1)
                {
                    $data['attachment3'] = $attach['attachment3'];
                }
//                var_dump($data['attachment3']); die;
                $this->_update($update_id, $data);
            } else {
                $nextid = $this->get_id();
                $uploadattachment = $this->do_upload($nextid, $data);
                $data['attachment1'] = $uploadattachment['1']['upload_data']['file_name'];
                $data['attachment2'] = $uploadattachment['2']['upload_data']['file_name'];
                $data['attachment3'] = $uploadattachment['3']['upload_data']['file_name'];
                $this->_insert($data);
            }

            redirect('admin/organization');
        }
    }
    function do_upload($id, $data) {
        $this->load->library('upload');
        $files = $_FILES;
        for ($i = 1; $i <= 3; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i - 1];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i - 1];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i - 1];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i - 1];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i - 1];
            
            $this->upload->initialize($this->set_upload_options($id, $i));
            $this->upload->do_upload();
            $datas[$i] = array('upload_data' => $this->upload->data());
//            $image_thumbnail = $this->image_optimization($id);
        }
//        echo '<pre>',print_r($datas),'</pre>'; die;

        return $datas;
    }

    function set_upload_options($id, $i) {

        $config = array(); 
        $config['upload_path'] = './uploads/organization/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '20480';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['file_name'] = $id.'attachment'.$i;
        return $config;
    }
        function get_id(){
	$this->load->model('mdl_organization');
	$id = $this->mdl_organization->get_id();
	return $id;
	}
	

    function get_district() {
        $this->load->model('district/mdl_district');
        $district_id = $this->mdl_district->get_district();
        return $district_id;
    }
    function get_working(){
        $this->load->model('working/mdl_working');
        $working_id=  $this->mdl_working->get_working();
     
        return $working_id;
    }
        function get_category() {
        $this->load->model('category/mdl_category');
        $category_id = $this->mdl_category->get_category();
        return $category_id;
    }
    function get_services(){
        $this->load->model('services/mdl_services');
        $services=$this->mdl_services->get_services();
        return $services;
    }

    function get($order_by) {
        $this->load->model('mdl_organization');
        $query = $this->mdl_organization->get($order_by);
        return $query;
    }

    function get_where($id) {
        $this->load->model('mdl_organization');
        $query = $this->mdl_organization->get_where($id);
        return $query;
    }

    function _insert($data) {
        $this->load->model('mdl_organization');
        $this->mdl_organization->_insert($data);
    }

    function _update($id, $data) {
        $this->load->model('mdl_organization');
        $this->mdl_organization->_update($id, $data);
    }

    function _delete($id) {
        $this->load->model('mdl_organization');
        $this->mdl_organization->_delete($id);
    }

    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_id_from_modulename($modulename) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }

}
