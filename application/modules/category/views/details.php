<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Khojam.com</title>
        <meta charset="utf-8">
        <link href="css/sp-goodies.v1.0.23.min.css" type="text/css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="sp-profile-page">
             <nav class="navbar navbar-default navbar-fixed-top sp-navbar-top" role="navigation" >
            <div class="container-fluid">
                <div class="row row-offcanvas row-offcanvas-right">
                    <div class="container-fluid">
                        <div class="navbar-header col-lg-2">
                            <div class="logo">
                                <img src="<?php echo base_url();?>/design/frontend/img/logo.png" style="padding: 3px 0px; width: 200px;">    
                            </div>
                            <button type="button" class="navbar-toggle" data-toggle="offcanvas"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a href="http://www.sodhpuch.com/#" class="sp-nav-srh-trg visible-xs" data-toggle="modal" onclick="doko.nsftv(event, this);"><span class="glyphicon glyphicon-search"></span></a>
                            <a class="navbar-brand brand-nohome hide" href="http://www.sodhpuch.com/"><img class="sp-logo" src="images/sp-logo.png" alt="sp-logo">
                            </a>
                        </div>

                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-7 sp-nav-search">
                            <div class="sp-nav-search-xs hidden-xs" id="sp-__fgds">
                                <div class="row" style="position:relative;">
                                    <div class="xs-modal-search">
                                        <form class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs" role="search" id="__fgds" method="get" action="http://www.sodhpuch.com/search/" onsubmit="return doko.__fgsps();">
                                            <div class="form-group col-xs-12 col-sm-6" id="search-q"> 
                                                <i class="fa fa-search"></i> 
                                                <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" data-toggle="popover" autocomplete="off" value="" class="col-xs-12 form-control input-lg sp-input-nav-search tt-hint" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" data-toggle="popover" name="q" autocomplete="off" value="" id="q" class="col-xs-12 form-control input-lg sp-input-nav-search tt-input" placeholder="Search Keywords" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-q"></div></span></span>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-5" id="search-lt"> <span class="twitter-typeahead" style="position: relative; display: inline-block; direction: ltr;"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-hint" autocomplete="off" value="" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input" autocomplete="off" name="_lt" id="_lt" value="" placeholder="Location" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-_lt"></div></span></span> <i class="fa fa-map-marker"></i> </div>
                                            <div class="form-group col-xs-12 col-sm-1">
                                                <input type="hidden" name="_qt" id="_qt" maxlength="1" value="A">
                                                <input type="hidden" name="_rt" id="_rt" maxlength="1" value="">
                                                <input type="hidden" name="l" id="l" maxlength="100" value="">
                                                <button type="submit" class="btn btn-lg sp-btn-nav-search sp-btn-go"><span class="glyphicon glyphicon-search hidden-xs"></span><span class="visible-xs">Search</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-3">
                            <div class="col-sm-5" style="padding:0px;">
                                <ul class="list-inline nav navbar-nav navbar-left sp-navbar-main hidden-xs">
                                    <li><a href="#"><i class="fa fa-home" style="color:#fff; font-size: 19px;"></i></a>
                                    </li>
                                    <li><a href="#"> <i class="fa fa-bars" style="color:#fff; font-size: 19px;"></i> </a>
                                </ul>
                            </div>
                            <div class="col-sm-7">
                                <div>
                                    <i class="fa fa-phone" style="color: #fff; padding-top: 10px;"> &nbsp;+97111111111</i>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    <i class="fa fa-envelope-o" style="color: #fff; padding-top: 5px;"> &nbsp;abc@gmail.com</i>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-offcanvas row" style="height:auto;">
                            <div class="container-fluid">
                                <p class="navbar-text navbar-right sp-tf tooltip-trg" data-original-title="Sodhpuch Toll Free Number&lt;br/&gt;This service is currently available between&lt;br/&gt;&lt;span class=&#39;tf-tth&#39;&gt;8:00 AM - 8:00 PM&lt;/span&gt;" data-toggle="tooltip" data-placement="bottom"><strong><i class="fa fa-phone-square sp-mrgR5"></i>16600 197 197</strong>
                                </p>

                                <ul class="nav navbar-nav navbar-right sp-navbar-main-right">
                                    <li class="visible-xs"><a href="#" title="About Sodhpuch Pvt. Ltd.">Home</a>
                                    </li>
                                    <li class="visible-xs"><a href="#" title="Explore Sodhpuch Discount Offers">About Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- ..header-->
        <div class="row row-offcanvas row-offcanvas-right">
            <!--..notice-->
            <div class="container" id="doko-error-box"> </div>
            <!--..notice-->
            <div class="sp-page sp-container-box">
                <div class="container" style="margin-top: 0px; margin-bottom: 0px;">

                    <div id="accueil">
                        <h1>District</h1>
                        <h2>Category</h2>
                    </div>
                    <div id="gallery">
                         <?php foreach ($district_data as $row){?>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <
                                <a href="<?php echo base_url();?>front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/1/" style="position: relative;" />
                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/2/"style="position: relative;" />
                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/3/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/4/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/2/" style="position: relative;"/>

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/4/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/1/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/3/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="front.html">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/1/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/2/" style="position: relative;"/>

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/3/" style="position: relative;"/>

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/4/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/2/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/4/" style="position: relative;" />
                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/1/" style="position: relative;" />

                                </a>
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                        <figure class="col-md-3 col-sm-3 col-xs-12 " style="padding:0px;">
                            <div class="grow pic">
                                <a href="#">
                                    <img class="img" src="http://lorempixel.com/800/400/nature/3/" style="position: relative;" />
                                </a>
                                
                                <h3 class="img-text">District name</h3>
                            </div>
                        </figure>
                    </div>
                </div>
                <!-- footer wrapper -->
                <!-- Begin footer -->
                <footer>
                    <div class="col-xs-12 footer">
                        <nav class="navbar sp-navbar-footer" role="navigation">
                            <div class="container-fluid">
                                <div class="col-md-5">
                                    <h4>Copyright,All rights reserved! ï¿½ 2015</h4>
                                </div>
                                <div class="col-md-4">
                                    <h4>Website Visited:
                                        <a href="http://www.freecounterstat.com" title="website counter"><img src="http://counter7.fcs.ovh/private/freecounterstat.php?c=dfb812b472759253fa94e0fead449279" border="0" title="website counter" alt="website counter"></a>
                                    </h4>
                                </div>
                                <div class="col-md-3">
                                    <div class="icon-social social">
                                        <ul class="icons pull-right">
                                            <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                                        </ul>
                                    </div>        
                                </div>
                            </div>
                        </nav>
                    </div> <a href="https://plus.google.com/101275354865770545665" rel="publisher" class="hide">Google+</a> </footer>
                <!-- Sodhpuch Overlay -->
                <div id="__doko-loading">Processing, please wait...</div>
                <div class="hidden" id="ab1-popover-tl"><span class="text-danger">Login Required!</span>
                </div>
                <div class="hidden" id="ab1-popover-bd"><small><p class="mrgD5">Please <a href="http://www.sodhpuch.com/login/?r=http%3A%2F%2Fwww.sodhpuch.com%2Fnepal-bank-ltd."><strong>login</strong></a> to perform this activity</p><p class="mrgD0"><strong>New to Sodhpuch?</strong></p><p><a href="http://www.sodhpuch.com/signup">Sign Up</a> | <a href="http://www.sodhpuch.com/register-company">Register Your Business</a>.</p></small>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="doko-modal-box" tabindex="-1" role="dialog" aria-labelledby="doko-modal-title" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">ï¿½</button>
                                <h4 class="modal-title" id="doko-modal-title">Modal title</h4> </div>
                            <div class="modal-body" id="doko-modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="doko-modal-submit" onclick="javascript:void(0);" data-loading-text="Processing...">Save changes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </body>

</html>