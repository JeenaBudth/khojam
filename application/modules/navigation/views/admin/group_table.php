<div class="manage">
    <input type="button" value="Add Group" id="create" onclick="location.href='<?php echo base_url()?>admin/navigation/create_group';"/> 
</div>
<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> Navigation Group </h4> 
	</div>
    

    <div class="widget-content"> 
		<table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Navigation Group</th>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr>                 	
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><a href="<?php echo base_url()?>admin/navigation/group/<?php echo $row->id;?>"><?php echo $row->title;?></a></td> 
                    <td class="edit">
						<?php if($row->slug != 'header' && $row->slug != 'footer'){?>
                            <a href="<?php echo base_url()?>admin/navigation/create_group/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>
                            &nbsp;&nbsp;/&nbsp;&nbsp; 
                            <a href="<?php echo base_url()?>admin/navigation/delete_group/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete this group and all navigation inside it?');"><i class="icon-trash"></i></a>
                        <?php } ?>
                  </td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table>
    </div>

</div><!--end of class="widget box"-->
