
         
                    <div class="container">
                        <div id="section-features" class="heading-block title-center page-section">
                            <h2 style="color:#DAA500">Telephone Number List</h2>
                        </div>
                        <link rel="stylesheet" href="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.css" >
                        <script type="text/javascript" src="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.js"></script>
                        <script>
                        $(document).ready(function(){
                            $('#myTable1').dataTable();
                        });
                        </script>
                       

<div class="widget-content"> 
    
        <table class="table table-striped table-bordered" id="myTable1">

            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th> 
                    <th data-class="expand">Phone Number</th> 
                    <th data-class="expand">Address</th> 
                    <th data-class="expand">District</th>
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($telephone_details->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo Ucfirst($row->name);?></td>
                     <td><?php echo Ucfirst($row->number);?></td>
                   <td><?php echo Ucfirst($row->address);?></td>
                   <td><?php echo Ucfirst($row->district);?></td>
                  
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>
                    </div>
                    <script>
                    $(function() {
                    $("#side-navigation").tabs({ show: { effect: "fade", duration: 400 } });
                    });                  </script>
 