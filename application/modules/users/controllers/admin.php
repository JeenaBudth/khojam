<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('groups'); //module name is groups here	
	}
	
		
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['username'] = $this->input->post('username', TRUE);		
		$data['email'] = $this->input->post('email', TRUE);		
		$data['display_name'] = $this->input->post('display_name', TRUE);	
		$data['group_id'] = $this->input->post('group_id', TRUE);		
		
			$update_id = $this->input->post('update_id', TRUE);
			if(!is_numeric($update_id))//
			{						
				$data['password'] = $this->input->post('password', TRUE);
				//$pwd = $this->zenareta_pasaoro($pwd);
				$data['created_on'] = date("Y-m-d");
				$data['activation_code'] = NULL;
				$data['last_login'] = NULL;
			}
			
		return $data;
	}
		
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['username'] = $row->username;	
			$data['email'] = $row->email;
			$data['display_name'] = $row->display_name;
			$data['group_id'] = $row->group_id;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		$submit = $this->input->post('submit', TRUE);
	
		if($submit=="Submit"){
			//person has submitted the form
			$data = $this->get_data_from_post();
		} 
		else {
			if (is_numeric($update_id)){
				$data = $this->get_data_from_db($update_id);
			}
		}
		
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
		
		$data['update_id'] = $update_id;
		$data['group_array'] = $this->get_groups();
				
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_users');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/users');
			}
		else
		{
			$this->mdl_users->_delete($delete_id);
			redirect('admin/users');
		}			
			
	}	
	
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('username', 'Username', 'required|xss_clean'); //we don't want unique_validation error while editing
			$this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean');
			$this->form_validation->set_rules('display_name', 'Display Name', 'required|xss_clean');
		}
		else{
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean|is_unique[up_users.username]'); //unique_validation check while creating new
		$this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean|is_unique[up_users.email]');
		$this->form_validation->set_rules('display_name', 'Display Name', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		}
		/*end of validation rule*/
		
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			
		
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$pwd = $data['password'];
				$data['password'] = $this->zenareta_pasaoro($pwd);//generating pwd
				$this->_insert($data);
			}
			
			redirect('admin/users');
		}
			
	}	
	
	
					
	function chpwd()
	{
		$update_id = $this->uri->segment(4);
		if(isset($update_id) && is_numeric($update_id))
			{
				$chpwd_submit = $this->input->post('submit', TRUE);
						
				if($chpwd_submit=="Submit")
					{
					//person has submitted the form
					$chpwd = $this->get_chpwd_from_post();
					} 
						
				if(!isset($chpwd))
					{
					$chpwd = $this->get_data_from_post();
					}
						
				$chdata['update_id'] = $update_id;										
				$chdata['password'] = $chpwd;
				
				
				$chdata['view_file'] = "admin/chpwd";
				$this->load->module('template/admin_template');
				$this->admin_template->admin($chdata);	
				
			}
		else
			{
				redirect('admin/users');
			}	
	}


	function chpwd_submit()
	{
				
	$this->load->library('form_validation');

	$this->form_validation->set_rules('chpwd', 'New Password', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			$this->chpwd();
		}
		else
		{
			$chdata['password'] = $this->get_chpwd_from_post();
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){		
			
				$pwd = $chdata['password'];
				$chdata['password'] = $this->zenareta_pasaoro($pwd);
				$this->chpwd_update($update_id, $chdata);
			} else {
				redirect('admin/users');
			}
			
			redirect('admin/users');
		}
			
	}
		
		
	function get_chpwd_from_post()
	{
		$chpwd = $this->input->post('chpwd', TRUE);
		return $chpwd;
	}

	
	
	
	
	function get($order_by){
	$this->load->model('mdl_users');
	$query = $this->mdl_users->get($order_by);
	return $query;
	}
	
	function get_groups()
	{
	$this->load->model('groups/mdl_groups');
	$query = $this->mdl_groups->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_users');
	$query = $this->mdl_users->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_users');
	$this->mdl_users->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_users');
	$this->mdl_users->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_users');
	$this->mdl_users->_delete($id);
	}
	
	function chpwd_update($id, $chpwd){
	$this->load->model('mdl_users');
	$this->mdl_users->chpwd_update($id, $chpwd);
	}
	
	function zenareta_pasaoro($pasaoro){		
	$this->load->model('admin_login/mdl_admin_login');
	$query = $this->mdl_admin_login->enc_hash_pwd($pasaoro);
	return $query;		
	}
	
	function sidebar($data){
	echo 'syo';
	}
	
}