<div class="row"> 
    <div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <h4><i class="icon-reorder"></i> District</h4> 
            </div> 
            <div class="widget-content">
                <?php
                echo validation_errors('<p style="color: red;">', '</p>');
                echo form_open_multipart('admin/district/submit', 'class="form-horizontal row-border" id="validate-1"');
                ?>                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Name Nepali <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('dis_name', $dis_name, 'class="form-control required"'); ?>
                    </div> 
                </div>
                     <div class="form-group"> 
                    <label class="col-md-2 control-label">Name English <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php echo form_input('dis_name_en', $dis_name_en, 'class="form-control required"');
                        //  $options = $all_district;
//                        echo form_dropdown('district', $options, $dis_name_en); ?>
                    </div> 
                </div>
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Attachment(180*156)px <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php
                            if (!empty($update_id)) {

                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile',
                                    'value' => $attachment
                                );
                            } else {
                                $attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile',
                                    'value' => $attachment,
                                    'class' => 'required'
                                );
                            }
                            ?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/district/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>
                       
                <div class="form-actions"> 
                    <?php
                    echo form_submit('submit', 'Submit', 'class="btn btn-primary pull-right"'); //name,value...type is default submit 
                if (!empty($update_id)) {
                       echo form_hidden('update_id', $update_id);
                    }
                    ?>
                </div>                 

<?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>