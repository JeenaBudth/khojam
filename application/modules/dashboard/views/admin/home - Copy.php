<!-- Dashboard -->




<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> Responsive Table </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>

    <div class="widget-content"> 
        <table class="table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column"><input type="checkbox" class="uniform"></th> 
                    <th data-class="expand">First Name</th> 
                    <th>Last Name</th> 
                    <th data-hide="phone">Username</th> 
                    <th data-hide="phone,tablet">Status</th> 
                </tr> 
            </thead> 
            <tbody> 
                <tr> 
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 
                    <td>Joey</td> 
                    <td>Greyson</td> 
                    <td>joey123</td> 
                    <td><span class="label label-success">Approved</span></td> 
                </tr> 
                <tr> 
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Wolf</td> 	
                    <td>Bud</td> 	
                    <td>wolfy</td> 	
                    <td><span class="label label-info">Pending</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Darin</td> 	
                    <td>Alec</td> 	
                    <td>alec82</td> 	
                    <td><span class="label label-warning">Suspended</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Andrea</td> 	
                    <td>Brenden</td> 	
                    <td>andry</td> 	
                    <td><span class="label label-danger">Blocked</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Joey</td> 	
                    <td>Greyson</td> 	
                    <td>joey123</td> 	
                    <td><span class="label label-success">Approved</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Wolf</td> 	
                    <td>Bud</td> 	
                    <td>wolfy</td> 	
                    <td><span class="label label-info">Pending</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Darin</td> 	
                    <td>Alec</td> 	
                    <td>alec82</td> 	
                    <td><span class="label label-warning">Suspended</span></td>	
                </tr> 	
                <tr> 	
                    <td class="checkbox-column"><input type="checkbox" class="uniform"></td> 	
                    <td>Andrea</td> 	
                    <td>Brenden</td> 	
                    <td>andry</td> 	
                    <td><span class="label label-danger">Blocked</span></td> 
                </tr> 
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->