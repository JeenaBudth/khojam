<div class="bs-docs-section">
    <div class="bs-glyphicons">
        <ul class="bs-glyphicons-list">
            <fieldset class="fsStyle">
<!--                <legend class="legendStyle">
                            <a data-toggle="collapse" data-target="#demo" href="#">Manage Success Stories</a>
                 </legend>-->
                    <a href="<?php echo base_url();?>admin/organization">
                <li>
                    <span class="glyphicon glyphicon-plus"></span>
                    <span class="glyphicon-class">Manage Organization</span>
                </li>
                </a>
                <a href="<?php echo base_url();?>admin/category">
                <li>
                    <span class="glyphicon glyphicon-plus"></span>
                    <span class="glyphicon-class">Manage Category</span>
                </li>
                </a>
                
            </fieldset>
             <fieldset class="fsStyle">
                
                 <a href="<?php echo base_url();?>admin/services">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Services</span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/district">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage District</span>
                    </li>
                </a>
                  <a href="<?php echo base_url();?>admin/telephonedirectory">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage telephone directory</span>
                    </li>
                </a>
                  <a href="<?php echo base_url();?>admin/media">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Media </span>
                    </li>
                </a>
                  <a href="<?php echo base_url();?>admin/meroadhar">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage MeroAdhar </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/history">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage History </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/student_of_year">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Student of year </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/advertisement">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Advertisement </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/fmdirectory">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage FM Directory </span>
                    </li>
                </a>
                  <a href="<?php echo base_url();?>admin/about_us">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage About Us </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/embassy_of_nepal">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Embassy of Nepal </span>
                    </li>
                </a>
                  <a href="<?php echo base_url();?>admin/team_members">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Team Members </span>
                    </li>
                </a>
                 <a href="<?php echo base_url();?>admin/religious_culture">
                    <li>
                        <span class="glyphicon glyphicon-plus"></span>
                        <span class="glyphicon-class">Manage Religious culture </span>
                    </li>
                </a>
            </fieldset>
        </ul>
    </div>
</div>