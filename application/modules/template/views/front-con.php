<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $site_settings['site_name'];?></title>
<meta name="title" content="<?php echo $site_settings['site_name'];?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic'];?>">
<meta name="description" content="<?php echo $site_settings['meta_data'];?>">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">
         <!--social icon-->
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-social.css">
<!--<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-social.less">-->
<link rel="stylesheet" href="<?php echo base_url();?>design/admin/css/fontawesome/font-awesome.min.css"> 
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/font-awesome.css">
<!--social icon end-->
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">

<!--<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">-->
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">
<!--<link href="<?php echo base_url();?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>  -->
<!--<link rel="stylesheet" href="<?php echo base_url();?>design/admin/css/fontawesome/font-awesome.min.css">--> 
<link href="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet" type="text/css" />

<?php if($this->uri->segment(2)!='view_final'){
    ?>
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<script src="<?php echo base_url();?>design/frontend/js/jquery.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/responsive-slider.js"></script>
			<script src="<?php echo base_url();?>design/frontend/js/jquery.event.move.js"></script>
<?php }else {?>
                        
                        <link href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
<script type="text/javascript" src="<?php echo base_url();?>design/admin/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script> 
                <link href="<?php echo base_url(); ?>design/frontend/css/rating.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/rating.js"></script>
<?php }?>
</head>
<body>
   <nav class="navbar navbar-default navbar-fixed-top sp-navbar-top" role="navigation" >
            <div class="container-fluid">
                <div class="row row-offcanvas row-offcanvas-right">
                    <div class="container-fluid">
                        <div class="navbar-header col-lg-2">
                            <div class="logo">
                                <a href="<?php echo base_url();?>" >
                                <img src="<?php echo base_url();?>design/frontend/img/logo.png" style="padding: 3px 0px; width: 200px;">    
                                </a>
                            </div>
                            <button type="button" class="navbar-toggle" data-toggle="offcanvas"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> <a href="http://www.sodhpuch.com/#" class="sp-nav-srh-trg visible-xs" data-toggle="modal" onclick="doko.nsftv(event, this);"><span class="glyphicon glyphicon-search"></span></a>
                            <a class="navbar-brand brand-nohome hide" href=""><img class="sp-logo" src="<?php echo base_url();?>design/frontend/images/sp-logo.png" alt="sp-logo">
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 sp-nav-search">
                            <div class="sp-nav-search-xs hidden-xs" id="sp-__fgds">
                                <div class="row" style="position:relative;">
                                    <div class="xs-modal-search">
                                        <?php echo form_open_multipart('organization/search', 'class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs"'); ?>  
                                        <!--<form class="navbar-form sp-frm-nav-search sp-frm-nav-search-xs" role="search" id="__fgds" method="get" action="#" onsubmit="return doko.__fgsps();">-->
                                            <div class="form-group col-xs-12 col-sm-6" id="search-q"> 
                                                <i class="fa fa-search"></i>
                                                                                    <?php echo form_input(array(
  'name' => 'organization',
  'value' => '',
  'placeholder' => 'Eg:Upveda Tech Pvt. Ltd.',
   'class' => 'col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input',    
   'id'=>'org',                                                                                  
)); ?>
                                                <!--<span class="twitter-typeahead" style="width: 100%;position: relative; display: inline-block; direction: ltr;"><input type="text" data-toggle="popover" autocomplete="off" value="" class="col-xs-12 form-control input-lg sp-input-nav-search tt-hint" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" data-toggle="popover" name="q" autocomplete="off" value="" id="q" class="col-xs-12 form-control input-lg sp-input-nav-search tt-input" placeholder="Search Keywords" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;"><div class="tt-dataset-q"></div></span></span>-->
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-5" id="search-lt" style="padding:0px;"> 
                                                <span class="twitter-typeahead" style=" width: 100%; position: relative; display: inline-block; direction: ltr;">
                                       <?php echo form_input(array(
  'name' => 'location',
  'value' => '',
  'placeholder' => 'Location',
   'class' => 'col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input',  
    'id' => 'loc',                                     
)); ?>                                                    
<!--<input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-hint" autocomplete="off" value="" disabled="" spellcheck="false" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);"><input type="text" class="col-xs-12 form-control input-lg sp-input-nav-search sp-search-location tt-input" autocomplete="off" name="_lt" id="_lt" value="" placeholder="Location" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: auto; text-transform: none;"></pre><span class="tt-dropdown-menu" style="position: absolute; top: 100%; z-index: 100; display: none; left: 0px; right: auto;">-->
                                                        <div class="tt-dataset-_lt"></div>
         
                                                    </span></span> <i class="fa fa-map-marker"></i> 
      
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-1">
                                                                 <?php     echo form_button(array(
  'name' => 'submit',
   'type'  => 'submit' ,                                                               
   'class' => 'nav-btn btn btn-lg sp-btn-nav-search sp-btn-go glyphicon glyphicon-search',
   'id'=>'btn1'
)); ?>
                                            </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-4">
                            <div class="col-sm-4" style="padding:0px;">
                                <ul class="list-inline nav navbar-nav navbar-left sp-navbar-main hidden-xs">
                                    <li><a href="#"><i class="fa fa-home" style="color:#fff; font-size: 19px;"></i></a>
                                    </li>
                                    <li><a href="#"> <i class="fa fa-bars" style="color:#fff; font-size: 19px;"></i> </a>
                                </ul>
                            </div>
                            <div class="col-sm-5">
                               <div>
                                    <i class="fa fa-phone" style="color: #fff; padding-top: 10px;"> &nbsp;+97111111111</i>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    <i class="fa fa-envelope-o" style="color: #fff; padding-top: 5px;"> &nbsp;abc@gmail.com</i>
                                </div>
                            </div>
                             <div class="col-sm-3">
                            <img src="<?php echo base_url();?>design/frontend/images/nepal_flag.gif" style="height:60px !important">
                        </div>
                        </div>
                       
                        <div class="sidebar-offcanvas row" style="height:auto;">
                            <div class="container-fluid">
                                <p class="navbar-text navbar-right sp-tf tooltip-trg" data-original-title="Sodhpuch Toll Free Number&lt;br/&gt;This service is currently available between&lt;br/&gt;&lt;span class=&#39;tf-tth&#39;&gt;8:00 AM - 8:00 PM&lt;/span&gt;" data-toggle="tooltip" data-placement="bottom">
                                </p>

                                <ul class="nav navbar-nav navbar-right sp-navbar-main-right">
                                    <li class="visible-xs"><a href="#" title="#">Home</a>
                                    </li>
                                    <li class="visible-xs"><a href="#" title="#">About Us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    	<div class="clearfix"></div>
    				<?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
        <img src="<?php echo base_url();?>design/frontend/images/khojam.png" class="img-responsive" style="margin-top: 60px;">
									
<!--								
				============================================start of responsive slider============================
					
						<div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
					<div class="slides" data-group="slides">
						<ul>
							<?php foreach($banner as $row){ //displaying all banner ?> 
								<li>
								  <div class="slide-body" data-group="slide">
                                                                      <img src="<?php echo base_url();?>uploads/banner/<?php echo $row['attachment1']?>" class="img-responsive" style="width:1500px;">
								  </div>
								</li>
							<?php }?>
						</ul>
					</div>
					<a class="slider-control left" href="#" data-jump="prev"><</a>
					<a class="slider-control right" href="#" data-jump="next">></a>
					
			   </div>
			  
				<script src="<?php echo base_url();?>design/public/js/responsive-slider.js"></script>
			<script src="<?php echo base_url();?>design/public/js/jquery.event.move.js"></script>
			 
			  ==============================================end of responsive slider============================   
				 start of top destination -->
                                    <!-- Content
               ============================================= -->
            <section id="content">
                <div class="content-wrap">
                    <div class="container clearfix">
                        <div id="section-features" class="heading-block title-center page-section">
                            <h2 style="color:#DAA500">Categories</h2>
                        </div>

                        <!-- square image -->
                        <div class="category">
                            <?php foreach ($category_data as $row){?>
                            <div class="col-sm-6 col-md-2">
                                <div class="thumbnail grow">
                                    <a href="<?php echo base_url();?>organization/details/<?php echo $row['slug'];?>"><img class="img-responsive" src="<?php echo base_url();?>uploads/category/<?php echo $row['attachment1'];?>"  alt="..."></a>
                                    <div class="caption img-caption">
                                        <h6 class="img-title"><?php echo $row['name'];?></h6>

                                    </div>
                                </div>
                            </div>
                            <?php }?>    
                            
                        </div>


                      
                        <!-- <div class="divider divider-short divider-center divider-gold"><i class="icon-circle"></i></div> -->

                    </div>
                    <script>
                    $(function() {
                    $("#side-navigation").tabs({ show: { effect: "fade", duration: 400 } });
                    });                  </script>
                </div>
            </section>
                                    <div class="col-md-12"  style="padding: 0px; margin-bottom: 30px; ">
                                        <div class="col-md-6" style="padding: 0px; margin-top:-60px; ">
				<!--============================================start of responsive slider============================-->
					
						<div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
					<div class="slides" data-group="slides">
						<ul>
							<?php foreach($banner_category as $row){ //displaying all banner ?> 
								<li>
								  <div class="slide-body" data-group="slide">
                                                                      <img src="<?php echo base_url();?>uploads/category/<?php echo $row['attachment2']?>" class="img-responsive" style="width:1500px;">
								  </div>
								</li>
							<?php }?>
						</ul>
					</div>
					<a class="slider-control left" href="#" data-jump="prev"><</a>
					<a class="slider-control right" href="#" data-jump="next">></a>
                                                </div>
			   </div>
                                <div class="col-md-3"  style="padding: 0px 2px;">
                                    <div class="fb-page" data-href="https://www.facebook.com/Khojamcom-980365968669569/?fref=ts" data-tabs="timeline" data-height="278" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Khojamcom-980365968669569/?fref=ts"><a href="https://www.facebook.com/Khojamcom-980365968669569/?fref=ts"></a></blockquote></div></div>
                                </div>
                                        <div class="col-md-3"  style="padding: 0px;">
                                            <a class="twitter-timeline"
                                                data-widget-id="600720083413962752"
                                                href="https://twitter.com/khojam2"
                                                
                                                height="250">

                                              </a>
                                        </div>
				
                      </div>
                                    
			  <!--==============================================end of responsive slider============================-->   
     
  <?php }?>
    <!-- #content end -->
    <!-- Footer
       ============================================= -->
<?php // echo 'helo'; die('asa'); ?>
    <!-- <footer id="footer" class="dark"> -->
    <?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
    <div class="last-bg">
        <div class="container" style="padding-top:15px; padding-bottom:15px;">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">
<!--                 style="margin-right:15px;" 
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 " >
                    <div class="media-title"><h4>Top Hotels</h4></div>
                    <div class="media grow-img" >
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" class="img-responsive" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea"/>
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Hotel Royal</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span> Durbar Marg,Kathmandu.</span></p>
                            <p class="bottom"> 
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Hotel Anna</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Thamel,Kathmandu.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Sangrilla</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Lazimpat,Kathmandu.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                </div>-->

                <!-- style="margin-right:30px;" -->
<!--                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 " >
                    <div class="media-title"><h4>Top Schools</h4></div>
                    <div class="media grow-img" >
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">St. Xavier College</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Maiti-ghar,Kathmandu.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Xavier International College</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Kalopul,Kathmandu.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Royal Palace</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Sanepa,Lalitpur.</span></p>
                            <p class="bottom"> 
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                </div>-->
<!--
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 ">
                    <div class="media-title"><h4>Top Restaurants</h4></div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Places Restaurant and Bar</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Satobato,Lalitpur.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                    <div class="media grow-img">
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Rosemary Fast-Food</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>D-square,Bhaktapur.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                                    <div class="media-body" style="padding:5px 0px 0px 5px;">
        <div class="media grow-img" >
                        <div class="media-left col-md-3" style="padding:0px;">
                            <a href="#">
                                <img class="media-object" src="<?php echo base_url();?>design/frontend/images/small/sea.jpg" width="74px" height="74px" alt="sea">
                            </a>
                        </div>
                        <div class="media-body" style="padding:5px 0px 0px 5px;">
                            <a href="#"><h5 class="media-heading">Frens Restaurant</h5></a>
                            <p class="bottom"> <i class="icon-map-marker icon"></i><span>Jhyatha,Kathmandu.</span></p>
                            <p class="bottom">
                                <input id="kartik" class="rating" data-stars="5" data-step="0.1"/>
                            </p>
                        </div>
                    </div>
                </div>

            </div>  .footer-widgets-wrap end -->

        </div>
    </div>
                                
                          
					<?php }
                                        else{?>
        
   
        <div class="col-lg-12" style="padding: 0px; margin-top: 60px; margin-bottom: 75px;">            
            <?php 
				if(!isset($view_file)){
					$view_file="";
				}
				if(!isset($module)){
					$module = $this->uri->segment(1);
				}
				if(($view_file!='') && ($module!='')){
					$path = $module."/".$view_file;
					$this->load->view($path);
				} 
				else {
					$new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
					echo nl2br($new_description);//put your designs here
				}
				
				//echo '<pre>';
				//print_r($this->session);
				//die();
            ?>
            </div>
                                       <?php }
                                        ?>
             <div class="clearfix"></div>
       
         <!-- Begin footer -->
    <footer>
        <div class="col-md-12 hidden-xs footer">
            <nav class="navbar sp-navbar-footer" role="navigation">
                <div class="container-fluid">
                    <div class="col-md-5">
                        <h4>Copyright,All rights reserved! Â© 2015</h4>
                    </div>
                    <div class="col-md-4">
                        <h4>Website Visited:
                            <a href="http://www.freecounterstat.com" title="website counter"><img src="http://counter7.fcs.ovh/private/freecounterstat.php?c=dfb812b472759253fa94e0fead449279" border="0" title="website counter" alt="website counter"></a>
                        </h4>
                    </div>
                    <div class="col-md-3">
                        <div class="icon-social social pull-right">
                            <ul class="icons">
                                <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                            </ul>
                        </div>        
                    </div>
                </div>
            </nav>
        </div> <a href="https://plus.google.com/101275354865770545665" rel="publisher" class="hide">Google+</a> </footer>
    <!-- End footer -->
        
       
	</div><!--end of container-->
<!--           <script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>-->
        <?php if($this->uri->segment(2)!='view_final'){?> 
        <script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>plugins/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
		<script src="<?php echo base_url();?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url();?>design/frontend/js/responsive-slider.js"></script>      
		<script src="<?php echo base_url();?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/main.js"></script> 
        
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    
    
</body>
</html>
<script>
    $(function(){
        $.ui.autocomplete.prototype._renderItem = function( ul, item){
  var term = this.term.split(' ').join('|');
  var re = new RegExp("(" + term + ")", "gi") ;
  var t = item.label.replace(re,"<strong>$1</strong>");
  return $( "<li></li>" )
     .data( "item.autocomplete", item )
     .append( "<a>" + t + "</a>" )
     .appendTo( ul );
};
var base_url='<?php echo base_url();?>';
        $("#loc").autocomplete({ 
    source:  base_url+"organization/get_location" // path to the get_o method
  });
  $("#org").autocomplete({ 
    source:  base_url+"organization/get_organization" // path to the get_o method
  });
});
    </script>
 
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));</script>
    <?php }?>