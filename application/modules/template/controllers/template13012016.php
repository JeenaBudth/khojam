<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function front($data){
		$data['site_settings'] = $this->get_site_settings();
		$data['banner'] = $this->get_banner();
		$data['headernav'] = $this->get_header('1');	//this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise
		$data['footernav'] = $this->get_footer('2');	//similar as above but 2 is for footer nav
		$data['category_data'] = $this->get_categoory_front();
//                var_dump($data['category_data']); die;
		//$data['sidebar'] = $this->get_navigation_from_navigation_name('sidebar'); 
		// ^ this line is to use navigation group for the future use by passing the parameter you can see in development code column of navigation group
		$this->load->view('front', $data);
	}
	
	function get_site_settings(){	
		$this->load->model('settings/mdl_settings');
		$query = $this->mdl_settings->get_settings();
		$result = $query->result_array();
		return $result[0];
	}	
	
	function get_navigation_from_navigation_name($navigation_name){		
		$this->load->model('navigation/mdl_navigation');
		$query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
		$result = $this->add_href($query->result_array());
		return $result;	
	}
	
	function errorpage(){
		$this->load->view('404');
	}
	
	function userlogin($data){
		$this->load->view('userlogin', $data);
	}
	
	function get_banner(){
	$this->load->model('banner/mdl_banner');
	$query = $this->mdl_banner->get_banner();
	return $query->result_array();
	}
	
	function get_parent($group_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_parentnav_for_frontend($group_id);	
	$result = $this->add_href($query);
	return $result;
	}
	
	function get_child($group_id,$parent_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_childnav_for_frontend($group_id,$parent_id);	
	$result = $this->add_href($query);
	return $result;
	}
	
	function get_header($group_id){
		$data['parentnav'] = $this->get_parent($group_id);	//
		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
				$nav['children'] = $children;
			}
		$navigation[$i] = $nav;
		$i++;	
		}
		return $navigation;
	}
	
	/*function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);
	return $query->result_array();
	}*/
	
	function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);	
	$result = $this->add_href($query->result_array());
	return $result;
	}
	
	
	
	
	function add_href($result){
		$count = count($result);
		for($i=0;$i<$count;$i++){			
				if($result[$i]['navtype'] == 'Module'){$result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'Page'){$result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'URI'){$result[$i]['href'] = $result[$i]['site_uri'];$result[$i]['target'] = "_self";}
				
				else{$result[$i]['href'] = $result[$i]['link_url'];$result[$i]['target'] = "_blank";}			
		}
		return $result;
	}
	
	
	
	function get_name_from_module($module_id){		
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_module($module_id);	
	return $query['0']['slug'];	
	}
	
	function get_name_from_page($page_id){		
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_page($page_id);
	return $query['0']['slug'];		
	}
        function get_categoory_front(){
	$this->load->model('category/mdl_category');
	$query = $this->mdl_category->get_category_front();
	return $query->result_array();
	}
}