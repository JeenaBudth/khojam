<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('team_members'); //module name is board_of_directors here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['title'] = $this->input->post('title', TRUE);			
		$data['address'] = $this->input->post('address', TRUE);
                $data['phone_no'] = $this->input->post('phone_no', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['attachment'] = $this->input->post('attachment', TRUE);			
		$data['gender'] = $this->input->post('gender', TRUE);
                $data['post'] = $this->input->post('post', TRUE);
                $data['meta_description'] = $this->input->post('meta_description', TRUE);
                $data['search_keys'] = $this->input->post('search_keys', TRUE);
                $data['title'] = $this->input->post('title', TRUE);
		$data['status'] = $this->input->post('status', TRUE);	
		$data['slug'] = strtolower(url_title($data['title']));
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
			
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
			$data['address'] = $row->address;
                        $data['phone_no'] = $row->phone_no;
                        $data['email'] = $row->email;
			$data['attachment'] = $row->attachment;
                        $data['gender'] = $row->gender;
                        $data['post'] = $row->post;
                        $data['meta_description'] = $row->meta_description;
                        $data['search_keys'] = $row->search_keys;
                        $data['title'] = $row->title;
			$data['status'] = $row->status;		
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_team_members');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/team_members');
			}
		else
		{
			$this->mdl_team_members->_delete($delete_id);
			redirect('admin/team_members');
		}				
	}
	
	
	function submit()
	{	
          
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
                      
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_team_members.name]'); //unique_validation check while creating new
		
                
                }
		/*end of validation rule*/	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){						
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];}
				
				$this->_update($update_id, $data);
			} else {								
					
				$nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				
				$this->_insert($data);	
			}
		
		redirect('admin/team_members');	
			
	}
	
	
	
	
	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/team_members/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_team_members');
	$id = $this->mdl_team_members->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_team_members');
	$query = $this->mdl_team_members->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_team_members');
	$query = $this->mdl_team_members->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_team_members');
	$this->mdl_team_members->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_team_members');
	$this->mdl_team_members->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_team_members');
	$this->mdl_team_members->_delete($id);
	}
}