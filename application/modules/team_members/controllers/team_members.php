<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_members extends MX_Controller
{
        function __construct() {
	parent::__construct();
	} 
        function index()
	{	
            $data['query'] = $this->get_bods('id');
            $data['view_file']="front";
            $this->load->module('template');
            $this->template->front($data);
	}
        function details()
	{ 
                $slug = $this->uri->segment(3);
                $query=$this->get_boddetails($slug);
//                var_dump($query->result());die('ff');
                foreach ($query->result() as $row) 
                {
                  $data['title'] = $row->title;
                  $data['attachment'] = $row->attachment;
                  $data['email'] = $row->email;
                  $data['phone_no'] = $row->phone_no;
                  $data['gender'] = $row->gender;
                  $data['description'] = $row->description;
                }               
                $data['view_file']="details";
                $this->load->module('template');
                $this->template->front($data);
	}
        function get_bods($order_by)
        {
            $this->load->model('mdl_board_of_directors');
            $query = $this->mdl_board_of_directors->get_bods($order_by);
            return $query;
	}
        function get_boddetails($slug){
	$this->load->model('mdl_board_of_directors');
	$query = $this->mdl_board_of_directors->get_boddetail($slug);
	return $query;
	}
}

	
        
