-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 06, 2014 at 06:42 AM
-- Server version: 5.1.32
-- PHP Version: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oopvedacmms`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_bod`
--

CREATE TABLE IF NOT EXISTS `up_bod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_no` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `working_place` varchar(255) NOT NULL,
  `expertize` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_bod`
--

INSERT INTO `up_bod` (`id`, `name`, `address`, `phone_no`, `email`, `attachment`, `description`, `working_place`, `expertize`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Harry', 'lele', 5445464, 'fdsa@sdss.sdf', '1.jpg', 'gsagsdgdfsgdsfgdfgdsfgdfgdsfgdfgdfgdsg\r\n', 'Upveda Technology', 'CodeIgniter', 'harry', '2014-08-06 00:00:00', NULL, 'live'),
(2, 'Dhruba kc', 'lele bn', 45545, 'ghfhfhf.com', '2.jpg', 'hehehehe\r\n', 'Upveda Technology', 'Java', 'dhruba-kc', '2014-08-06 00:00:00', '2014-08-06 00:00:00', 'live'),
(3, 'Hero', 'lele bn', 45545, 'fdsa@sdss.sdf', '3.jpg', 'fafasfsafsadf\r\n', 'Upveda Technology', 'Java', 'hero', '2014-08-06 00:00:00', NULL, 'live');
