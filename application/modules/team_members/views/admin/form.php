<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce_4/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
     
    tinymce.init({
      selector: "textarea",
      
      // ===========================================
      // INCLUDE THE PLUGIN
      // ===========================================
    	
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste jbimages",
         "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern imagetools"
      ],
    	
      // ===========================================
      // PUT PLUGIN'S BUTTON on the toolbar
      // ===========================================
    	
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | image",
      toolbar3: "print preview media | forecolor backcolor emoticons",
    	
      // ===========================================
      // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
      // ===========================================
      image_advtab: true,
	
      relative_urls: false
    	
    });
     
    </script>
<!-- /TinyMCE -->
<!-- /TinyMCE -->

<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i>Team Members</h4> 
            </div> 
            <div class="widget-content">
            	<?php
                 echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/team_members/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('title', $title, 'class="form-control required"');?>
			</div> 
                    </div>
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Gender</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $gender;$options = array(
                              'male'  => 'Male',
                              'female'    => 'Female',
                            );                            
                            echo form_dropdown('gender', $options, $selected,'class="form-control required"');?>
						</div> 
                    </div> 
                    
                   <div class="form-group"> 
                    	<label class="col-md-2 control-label">Address<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('address', $address, 'class="form-control required"');?>
			</div> 
                    </div>
                    
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Phone_No<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('phone_no', $phone_no, 'class="form-control required"');?>
			</div> 
                    </div>
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Email<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('email', $email, 'class="form-control required"');?>
			</div> 
                    </div>
                
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Photo(225*217) <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php  if(!empty($update_id)){
										
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment
												);
										}else{
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment,
												'class' => ''
												);
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/team_members/<?php if($attachment!=null && $attachment!='') {echo $attachment;}
                    else
                        {
                        if($gender=='female')
                        {
                            echo 'female.png';
                        }
                        else
                        {
                        echo 'male.png';
                        }

                        }
                        ?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>

             <div class="form-group"> 
                    	<label class="col-md-2 control-label">Post<span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('post', $post, 'class="form-control required"');?>
			</div> 
                    </div>
                
                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control required"');?>
						</div> 
                    </div>  
                    			
                  
                
                    
                    <div class="form-actions"> 
						<?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>