<div class="manage">
    <input type="button" value="Add Team Members" id="create" onclick="location.href='<?php echo base_url()?>admin/team_members/create';"/>  
</div>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i>Board of Directors </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th>
                    <th>Gender</th> 
                    <th>Address</th> 
                    <th>Phone_no</th>
                    <th>Email</th>
                    <th>Photo</th> 
                    <th>Status</th>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->title;?></td>
                    <td><?php echo $row->gender;?></td>
                    <td><?php echo $row->address;?></td>
                    <td><?php echo $row->phone_no;?></td>
                    <td><?php echo $row->email;?></td>
                    <td><img src="<?php echo base_url();?>uploads/team_members/<?php if($row->attachment!=null && $row->attachment!='') {echo $row->attachment;}
                    else
                        {
                        if($row->gender=='female')
                        {
                            echo 'female.png';
                        }
                        else
                        {
                        echo 'male.png';
                        }

                        }
                        ?>" style="height:50px;"/></td> 
                    
                    <td><?php echo $row->status;?></td>
					<td class="edit">
                        <a href="<?php echo base_url()?>admin/team_members/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>                   
                        &nbsp;&nbsp;/&nbsp;&nbsp; 
                        <a href="<?php echo base_url()?>admin/team_members/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
					</td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->