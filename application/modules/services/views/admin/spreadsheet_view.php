
<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldata.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border='1'>
  <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th> 
                    <th data-class="expand">Faculty</th> 
                    <th data-class="expand">Level</th> 
                    <th data-class="expand">Description</th>
                    
                </tr> 
            </thead> 
            
            <tbody>
                
                <?php $sno = 1;
            
            ?>
               <?php foreach($query->result() as $row){
                   ?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->name;?></td> 
                    <td><?php echo $row->department;?></td> 
                     <td><?php echo $row->level;?></td> 
                      <td><?php echo $row->description;?></td>
                      
                      
                      <?php }	?>   
           
            </tbody>
            
</table>
