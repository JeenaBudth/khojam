<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_services extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table ="up_services";
	return $table;
	} 
        function group_by(){
            $this->db->select('id','name');
            $this->db->order_by('name');
            foreach ($dropdowns as $dropdown)
                    {
                    $dropdownlist[$dropdown->id] = $dropdown->name;
                    }
                    return $dropdownlist;
        }
            function get_services(){
            $this->db->select('id, name');	
            $this->db->order_by('name');
            $dropdowns = $this->db->get('up_services')->result();
            foreach ($dropdowns as $dropdown)
                    {
                    $dropdownlist[$dropdown->id] = $dropdown->name;
                    }
            if(empty($dropdownlist)){return NULL;}
            $finaldropdown = $dropdownlist;
            return $finaldropdown;        
        }
        
//        function get_semester(){
//           
//	$this->db->select('id, name');	
//	$this->db->order_by('name');
//	$dropdowns = $this->db->get('up_semester')->result();
//	foreach ($dropdowns as $dropdown)
//		{
//		$dropdownlist[$dropdown->id] = $dropdown->name;
//		}
//	if(empty($dropdownlist)){return NULL;}
//	$finaldropdown = $dropdownlist;
//	return $finaldropdown;            
//        }
        
        function get($order_by){
	$table = $this->get_table();
        
//        $this->db->select('up_services.*');
//        $this->db->order_by('up_dropdown_practice.faculty','asc');
//	$this->db->select('up_faculty.name as faculty'); 
//	$this->db->join('up_faculty', 'up_faculty.id = up_dropdown_practice.faculty');
////        $this->db->select('up_club.club_name as supporting_club');
//        $this->db->join('up_club','up_club.id = up_dropdown_practice.supporting_club');
	$query=$this->db->get($table);
	//$this->db->order_by($order_by,'ASC');
//        var_dump($query->result()); die;
	return $query;
	}
        
//        function get_all(){
//	$table = $this->get_table();
//       // $this->db->select('up_services.*');
////	$this->db->select('up_faculty.name as faculty');
////	$this->db->join('up_faculty', 'up_faculty.id = up_dropdown_practice.faculty');
////	$this->db->select('up_club.club_name as supporting_club');
////	$this->db->join('up_club', 'up_club.id = up_dropdown_practice.supporting_club');
//	$this->db->order_by('id','ASC');
//	$query=$this->db->get($table);
////        $i=sizeof($query->result());
////        echo $i;
////        var_dump($query->result());
////        die('yaha aayo');
//	return $query;
//	}
        
	function get_where($id){
	$table = $this->get_table();
//        $this->db->select('up_dropdown_practice.*');
//	$this->db->select('up_country.name as country');
//	$this->db->join('up_country', 'up_country.id = up_dropdown_practice.country');
//	$this->db->select('up_club.club_name as supporting_club');
//	$this->db->join('up_club', 'up_club.id = up_dropdown_practice.supporting_club');
        $this->db->where('up_services.id', $id);
	$query=$this->db->get($table);
	return $query;
	}
        
//        function get_view($id){
//	$table = $this->get_table();
//        $this->db->select('up_dropdown_practice.*');
//	$this->db->select('up_faculty.name as faculty');
//	$this->db->join('up_faculty', 'up_faculty.id = up_dropdown_practice.faculty');
////	$this->db->select('up_club.club_name as supporting_club');
////	$this->db->join('up_club', 'up_club.id = up_dropdown_practice.supporting_club');
//        $this->db->where('up_dropdown_practice.id', $id);
//	$query=$this->db->get($table);
//	return $query;
//	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
        
        function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_services'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment'];   
	return $nextId;
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	$this->delete_permissions_of_that_module_too($id);
	}
	
	
	function delete_permissions_of_that_module_too($id){	
	$table = 'up_permissions';	
	$query=$this->db->get($table)->result();echo '<pre>';
		foreach($query as $row){
				$roles_array = unserialize($row->roles);
				foreach($roles_array as $check){
					if($check == $id){
						unset($roles_array[$id]);	
						$new_role = serialize($roles_array);										
						$this->db->where('id', $row->id);
						$this->db->update($table, array('roles' => $new_role));					
					}
				}
		}
	}
	
	
//	function get_Student_dropdown()
//	{
//	$this->db->select('id, title');	
//	$this->db->order_by('title');
//	$dropdowns = $this->db->get('up_learner')->result();
//	foreach ($dropdowns as $dropdown)
//		{
//		$dropdownlist[$dropdown->id] = $dropdown->title;
//		}
//	if(empty($dropdownlist)){return NULL;}
//	$finaldropdown = $dropdownlist;
//	return $finaldropdown;
//	}
	
//	function get_module_ids(){
//	$this->db->select('id');	
//	$this->db->order_by('id');
//	$dropdowns = $this->db->get('up_dropdown_practice')->result();
//	foreach ($dropdowns as $dropdown)
//		{
//		$dropdownlist[$dropdown->id] = $dropdown->id;
//		}
//	if(empty($dropdownlist)){return NULL;}
//	$finaldropdown = $dropdownlist;
//	return $finaldropdown;	
//	}
	
	function get_id_from_modulename($modulename){	
	$table = $this->get_table();		
	$this->db->select('id')	;
	$this->db->where('slug', $modulename);		
	$query=$this->db->get($table)->result();
	$result = $query[0]->id;
	return $result;
	}
	
	
	function get_all_slug_from_module(){	
	$table = $this->get_table();		
	$this->db->select('slug');	
	$query=$this->db->get($table)->result();
		$i = 1;
		foreach($query as $row){
			$new_array[$i] = $row->slug;
			$i++;				
		}
	return $new_array;
	}
	
	
	function get_slug_from_moduleid($module_id){	
	$table = $this->get_table();		
	$this->db->select('slug')	;
	$this->db->where('id', $module_id);		
	$query=$this->db->get($table)->result();
	$result = $query[0]->slug;
	return $result;
	}
//
//        function get_name($id){
//        $table = 'up_semester';
////        $this->db->select('up_dropdown_practice.*');
////	$this->db->select('up_country.name as country');
////	$this->db->join('up_country', 'up_country.id = up_dropdown_practice.country');
////	$this->db->select('up_club.club_name as supporting_club');
////	$this->db->join('up_club', 'up_club.id = up_dropdown_practice.supporting_club');
//        $this->db->where('up_semester.id', $id);
//	$query=$this->db->get($table);
//	return $query;
//       // var_dump($query);die;
//	}
        
}