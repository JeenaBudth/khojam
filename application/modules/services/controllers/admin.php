<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kathmandu');
        $this->load->module('admin_login/admin_login');
       $this->admin_login->check_session_and_permission('services'); //module name is learner here	
    }

    function index() {
        $modulename = $this->uri->segment(2);

        $data['module_id'] = $this->get_id_from_modulename($modulename);

        $group_id = $this->session->userdata['group_id']; //to set the permession of user group

        $data['permissions'] = $this->unserialize_role_array($group_id);
        $data['query'] = $this->get('id');
        $data['view_file']="admin/table";
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
     
       
    }

    function get_data_from_post() {
        $data['name'] = $this->input->post('name', TRUE);
        $data['slug']=strtolower(url_title($data['name']));
        return $data;
    }

    function get_data_from_db($update_id) {
        $query = $this->get_where($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
           
        }
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }
    
    function get_data_for_view($update_id) {
        $query = $this->get_view($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
           
        
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }
    }

    function create() {        
        $update_id = $this->uri->segment(4);
        //if($update_id != 1){
        $submit = $this->input->post('submit', TRUE);
        if ($submit == "Submit") {
            //person has submitted the form
            
            
            $data = $this->get_data_from_post();
        }
        else {
            if (is_numeric($update_id)) {
                $data = $this->get_data_from_db($update_id);
            }
        }
        if (!isset($data)) {
            $data = $this->get_data_from_post();
        }
//        $all_services=  $this->get_services();
//        $data['all_services']=$all_services;
       
        $data['update_id'] = $update_id;
        $data['module'] = 'services';
        $data['view_file'] = "admin/form";
        
//      var_dump($data['supporting_club']);die;
        
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
        // }
//		}else{
//		redirect('admin/groups');
    }
//    function group_by(){
//        $this->load->model('mdl_dropdown_practice');
//        $faculty_name=$this->mdl_dropdown_practice->group_by();
//        if($faculty_name=='BBA'){
//            echo'ksklf';
//            
//            
//        }
//    }
//                function get_faculty(){
//        $this->load->model('mdl_dropdown_practice');
//        $faculty = $this->mdl_dropdown_practice->get_faculty();    
//        return $faculty;
//    }
//    function get_semester(){
//        $this->load->model('mdl_dropdown_practice');
//        $semester = $this->mdl_dropdown_practice->get_semester();  
//        
//        return $semester;        
//        }


    function delete() {
        $this->load->model('mdl_services');
        $delete_id = $this->uri->segment(4);
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group

        if (!isset($delete_id) || !is_numeric($delete_id)) {
            unset($delete_id);
            redirect('admin/services');
        } else {
            if ($group_id != 1) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);
                if ((strpos($mystring, 'd' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $this->mdl_services->_delete($delete_id);
                    redirect('admin/services');
                }
            }
            $this->mdl_services->_delete($delete_id);
            redirect('admin/services');
        }
    }

    function view() {
        $group_id = $this->session->userdata['group_id']; //to set the permession of user group
        $view_id = $this->uri->segment(4);
        if ($group_id != 1) {
            if (is_numeric($view_id)) {
                $permissions = $this->unserialize_role_array($group_id);
                $modulename = $this->uri->segment(2);
                $module_id = $this->get_id_from_modulename($modulename);
                $mystring = implode(" ", $permissions);

                if ((strpos($mystring, 'v' . $module_id)) == false) {
                    redirect('admin/dashboard');
                } else {
                    $data = $this->get_data_for_view($view_id);
                }
            }
        }
        $data = $this->get_data_for_view($view_id);
        $data['view_id'] = $view_id;
        $data['view_file'] = "admin/view";
//        var_dump($data);
//        die;
        $this->load->module('template/admin_template');
        $this->admin_template->admin($data);
    }

    function submit() {
       
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			redirect('admin/services');
		}
			
    }
    

    function get_id() {
        $this->load->model('mdl_services');
        $id = $this->mdl_services->get_id();
        return $id;
    }


    function get($order_by) {
        $this->load->model('mdl_services');
        $query = $this->mdl_services->get($order_by);
        return $query->result();
    }

    function get_where($id) {
        $this->load->model('mdl_services');
        $query = $this->mdl_services->get_where($id);
        return $query;
    }

    function get_view($id) {
        $this->load->model('mdl_services');
        $query = $this->mdl_services->get_view($id);
        return $query;
    }
    
    function _insert($data) {
        $this->load->model('mdl_services');
        $this->mdl_services->_insert($data);
    }

    function _update($id, $data) {
        $this->load->model('mdl_services');
        $this->mdl_services->_update($id, $data);
    }

    function _delete($id) {
        $this->load->model('mdl_services');
        $this->mdl_services->_delete($id);
    }

    function unserialize_role_array($group_id) {
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }

    function get_id_from_modulename($modulename) {
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }

//    function get_groups_programs() {
//        $this->load->model('faculty/mdl_faculty');
//        $query = $this->mdl_faculty->get_faculty_dropdown();
//        if (empty($query)) {
//            return NULL;
//        }
//        return $query;
//    }
//    
    function get_name($id) {
        $this->load->model('mdl_services');
        $query = $this->mdl_services->get_name($id);
        return $query;
    }
    }
    
//    function do_upload($id, $data) {
//        $this->load->library('upload');
//        $files = $_FILES;
//        
//        for ($i = 1; $i <= 2; $i++) {
//
//            $_FILES['userfile']['name'] = $files['userfile']['name'][$i - 1];
//            $_FILES['userfile']['type'] = $files['userfile']['type'][$i - 1];
//            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i - 1];
//            $_FILES['userfile']['error'] = $files['userfile']['error'][$i - 1];
//            $_FILES['userfile']['size'] = $files['userfile']['size'][$i - 1];
//            
//            $this->upload->initialize($this->set_upload_options($id, $i));
//            $this->upload->do_upload();
//            $datas[$i] = array('upload_data' => $this->upload->data());
////            $image_thumbnail = $this->image_optimization($id);
//        }
////        echo '<pre>',print_r($datas),'</pre>'; die;
//
//        return $datas;
//    }
//    function set_upload_options($id, $i) {
//
//        $config = array(); 
//        $config['upload_path'] = './uploads/publication/';
//        $config['allowed_types'] = 'text/plain|text/csv|gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar|csv';
//        $config['max_size'] = '20480';
//        $config['overwrite'] = TRUE;
//        $this->load->library('upload', $config);
//        $this->upload->initialize($config);
//        $config['max_width'] = "1907";
//        $config['max_height'] = "1280";
//        $config['file_name'] = $id.'attachment'.$i;
//        if (!$this->upload->do_upload())
//        {
//            echo"unable to upload";
//            
//        }else{
//            echo'file uploaded';
//        }
//        return $config;
//    }
////    function do_upload($id) {
////        $config['upload_path'] = "./uploads/upload";
////        $config['file_name'] = $id;
////        $config['overwrite'] = TRUE;
////        $config['allowed_types'] = "gif|jpg|jpeg|png";
////        $config['max_size'] = "20480"; //that's 20MB
////        $config['max_width'] = "1907";
////        $config['max_height'] = "1280";
////        $this->load->library('upload', $config);
////        if (!$this->upload->do_upload()) {
////            echo 'File cannot be uploaded</br>';
////            $datas = array('error' => $this->upload->display_errors());
////            //$datas = array('upload_data' => $this->upload->data());	
////        } else {
////            echo 'File has been uploaded';
////            $datas = array('upload_data' => $this->upload->data());
////        }
////        return $datas;
////    }
//    function get_attachment_from_db($update_id){
//        $query=$this->get_where($update_id);
//        foreach($query->result() as $row)
//		{			
//			$data['attachment1'] = $row->attachment1;
//                        $data['attachment2']=$row->attachment2;
//                        
//		}
//			return $data;
//    }
//}
