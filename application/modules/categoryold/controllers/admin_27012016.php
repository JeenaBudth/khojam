<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('category'); //module name is name here	
	}
	
	function index()
	{	
            
            $modulename = $this->uri->segment(2);
            $data['module_id']=$this->get_id_from_modulename($modulename);
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $data['permissions'] = $this->unserialize_role_array($group_id);
            $data['query'] = $this->get('id');
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
            
		$data['name'] = $this->input->post('name', TRUE);
                $data['name_np']=$this->input->post('name_np',TRUE);
		$data['slug'] = strtolower(url_title($data['name']));
                  $data['description']=$this->input->post('description',TRUE);
                  $data['status']=$this->input->post('status',TRUE);
                   $data['is_for_banner']=$this->input->post('is_for_banner',TRUE);
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
                                $attach = $this->get_attachment_from_db($update_id);
				
			}
			else
			{
                            $data['attachment1']=$this->input->post('userfile',TRUE);
                            $data['attachment2']=$this->input->post('userfile',TRUE);
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment1'] = $row->attachment1;	
                        $data['attachment2'] = $row->attachment2;	
		}
			return $data;
	}	
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
                        $data['name_np']=$row->name_np;
                        $data['description']=$row->description;
                        $data['attachment1']=$row->attachment1;
                        $data['attachment2']=$row->attachment2;
                        $data['status']=$row->status;
                         $data['is_for_banner']=$row->is_for_banner;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	 function get_data_for_view($update_id) {
        $query = $this->get_view($update_id);
        foreach ($query->result() as $row) {
            $data['name'] = $row->name;
            $data['name_np']=$row->name_np;
            $data['description']=$row->description;
           
        
        if (!isset($data)) {
            $data = "";
        }
        return $data;
    }
    }
	
	function create()
	{       
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $update_id = $this->uri->segment(4);
            
		$submit = $this->input->post('submit', TRUE);
		if($submit=="Submit")
                    {
			//person has submitted the form
			$data = $this->get_data_from_post();
                    }else{
                        
			if (is_numeric($update_id))
                            {
                            $data=$this->get_data_from_db($update_id);
                    }
                    }
                          
            
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
                $data['module']='category';
                
		$data['update_id'] = $update_id;
		$data['view_file'] = "admin/form";
                
                
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_category');
		$delete_id = $this->uri->segment(4);
                $group_id = $this->session->userdata['group_id'];//to set the permession of user group
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/category');
			}
		else
		{
                    if($group_id!=1)
                    {
                    $permissions = $this->unserialize_role_array($group_id);
                    $modulename = $this->uri->segment(2);
                    $module_id=$this->get_id_from_modulename($modulename);
                    $mystring = implode(" ",$permissions);
                    if((strpos($mystring, 'd'.$module_id))==false)
                    {
                        redirect('admin/dashboard');  
                    }
                    else
                    {
                      	$this->mdl_category->_delete($delete_id);
			redirect('admin/category');  
                    }
                    }
			$this->mdl_category->_delete($delete_id);
			redirect('admin/category');
		}				
	}
	function view()
        {
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
		$view_id = $this->uri->segment(4);
		if($group_id != 1){
				if (is_numeric($view_id))
                                    {
                                        $permissions = $this->unserialize_role_array($group_id);
                                        $modulename = $this->uri->segment(2);
                                        $module_id=$this->get_id_from_modulename($modulename);
                                        $mystring = implode(" ",$permissions);
                                        
                                        if((strpos($mystring, 'v'.$module_id))==false)
                                        {
                                            redirect('admin/dashboard');  
                                        }
                                        else
                                        {
                                         $data = $this->get_data_from_db($view_id);   
                                        }
                                      }
                                    }
                        $data = $this->get_data_from_db($view_id);
			$data['view_id'] = $view_id;
			$data['view_file'] = "admin/view";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
                }
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else {
            $data = $this->get_data_from_post();

            $update_id = $this->input->post('update_id', TRUE);
            if (is_numeric($update_id)) {
                $attach = $this->get_attachment_from_db($update_id);
                $uploadattachment = $this->do_upload($update_id, $data);
                $data['attachment1'] = $uploadattachment['1']['upload_data']['file_name'];
                $data['attachment2'] = $uploadattachment['2']['upload_data']['file_name'];
                $result1=explode(".",$data['attachment1']);
                $result2=explode(".",$data['attachment2']);
                if(sizeof($result1)==1)
                {
                    $data['attachment1'] = $attach['attachment1'];
                }
                if(sizeof($result2)==1)
                {
                    $data['attachment2'] = $attach['attachment2'];
                }
                $this->_update($update_id, $data);
            } else {
                $nextid = $this->get_id();
                $uploadattachment = $this->do_upload($nextid, $data);
                $data['attachment1'] = $uploadattachment['1']['upload_data']['file_name'];
                $data['attachment2'] = $uploadattachment['2']['upload_data']['file_name'];
                $this->_insert($data);
            }

            redirect('admin/category');
		}
			
	}
        function do_upload($id, $data) {
        $this->load->library('upload');
        $files = $_FILES;
        for ($i = 1; $i <= 2; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i - 1];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i - 1];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i - 1];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i - 1];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i - 1];
            
            $this->upload->initialize($this->set_upload_options($id, $i));
            $this->upload->do_upload();
            $datas[$i] = array('upload_data' => $this->upload->data());
//            $image_thumbnail = $this->image_optimization($id);
        }
//        echo '<pre>',print_r($datas),'</pre>'; die;

        return $datas;
    }

    function set_upload_options($id, $i) {

        $config = array(); 
        $config['upload_path'] = './uploads/category/';
        $config['allowed_types'] = 'gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar|csv';
        $config['max_size'] = '20480';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $config['max_width'] = "1907";
        $config['max_height'] = "1280";
        $config['file_name'] = $id.'attachment'.$i;
        return $config;
    }
	function get_id(){
	$this->load->model('mdl_category');
	$id = $this->mdl_category->get_id();
	return $id;
	}	

	function get($order_by){
	$this->load->model('mdl_category');
	$query = $this->mdl_category->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_category');
	$query = $this->mdl_category->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_category');
	$this->mdl_category->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_category');
	$this->mdl_category->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_category');
	$this->mdl_category->_delete($id);
	}
        function unserialize_role_array($group_id){		
	$this->load->model('permissions/mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
        function get_id_from_modulename($modulename){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
	return $query;
        }
}