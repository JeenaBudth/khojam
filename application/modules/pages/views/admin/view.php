<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Pages</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo $title?>
						</div> 
                    </div>
                    
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                        	<?php echo $description?>
                        </div> 
					</div>
             
                    
                  
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>