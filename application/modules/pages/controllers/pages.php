<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function index(){
		$first_bit = $this->uri->segment(1);
		$second_bit = $this->uri->segment(2);
		
			if ($second_bit==""){
				//this must be a custom page from the CMS
				
					if($first_bit==""){
						$first_bit = "home";
						//$banner = $this->banner();
					}
			}
				
			$query = $this->get_pages_if_live('slug', $first_bit);
			
			foreach($query->result() as $row){
				
				$data['title'] = $row->title;					
				$data['slug'] = strtolower(url_title($data['title']));
				$data['description'] = $row->description;
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
				
			}
			
			if($query->num_rows == 0)
			{
				$this->load->module('template');
				$this->template->errorpage();
			}
			else
			{
				$this->load->module('template');
				$this->template->front($data);
			}	
                        
		
	}

	
	function get_pages_if_live($col, $value) {
	$this->load->model('mdl_pages');
	$query = $this->mdl_pages->get_pages_if_live($col, $value);
	return $query;
	}

	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_pages');
	$query = $this->mdl_pages->get_where_custom($col, $value);
	return $query;
	}

}