<div class="manage">
                    <?php 
                $group_id = $this->session->userdata['group_id'];
                if($group_id!=1)
                    {
                    $mystring = implode(" ",$permissions);
                    if ((strpos($mystring, 'a'.$module_id))==true) 
                    {?>
    <input type="button" value="Add Content" id="create" onclick="location.href='<?php echo base_url()?>admin/religious_culture/create';"/>
                     <?php
                     }
                    }
                     else
                     {?>
    <input type="button" value="Add Content" id="create" onclick="location.href='<?php echo base_url()?>admin/religious_culture/create';"/>
                    <?php
                     }
                     ?>

</div>
<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i>Religious and Culture</h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th> 
                   
                    <th data-class="expand">Description</th> 
                    <th data-class="expand">Image</th> 
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->nep_title;?></td>
                   
                    <td><?php echo $row->description;?></td> 
                    <td><img src="<?php echo base_url();?>uploads/religious_culture/<?php echo $row->attachment;?>" style="height:50px;"/></td> 
                    <td class="edit">
                    	<?php if($row->slug != 'home'){?>
                            <a href="<?php echo base_url()?>admin/religious_culture/view/<?php echo $row->id;?>"><?php if($group_id==1){echo '<i class="icon-eye-open"> / </i>';} elseif((strpos($mystring, 'v'.$module_id))==true){echo '<i class="icon-eye-open"> / </i>';} ?></a>                   
                            
                            <a href="<?php echo base_url()?>admin/religious_culture/create/<?php echo $row->id;?>"><?php if($group_id==1){echo '<i class="icon-pencil"> / </i>';} elseif((strpos($mystring, 'e'.$module_id))==true){echo '<i class="icon-pencil"> / </i>';} ?></a>                   
                            
                            <a href="<?php echo base_url()?>admin/religious_culture/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><?php if($group_id==1){echo '<i class="icon-trash"></i>';} elseif((strpos($mystring, 'd'.$module_id))==true){echo '<i class="icon-trash"></i>';} ?></a>
                        <?php } ?>
                  </td>
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->