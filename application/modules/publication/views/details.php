<div class="main-content col-md-12">
               <div class="success-stories">
                      <?php foreach($publication_details as $row) { ?>
                     <div class="success-wrapper">
                         <h3><?php echo $row['title'];?></h3>
                        <div class="pull-left img-success">
                            <img src="<?php echo base_url();?>uploads/publication/<?php echo $row['attachment1'];?>">
                        </div>
                        <h4 style="color:rgba(0, 0, 0, 0.76); font-weight:bold;">Document Summary</h4>
                        <p class="params"><?php echo $row['description'];?></p>
                        <div class="download_document pull-right" >
                            <center>
                            <h5>Download this Document</h5>
                            <a href="<?php echo base_url();?>uploads/publication/<?php echo $row['attachment2'];?>" target="blank" class="btn btn-info">Download</a>
                            </center>
                        </div>
                     </div>
                     <?php  } ?> 
                     <div class="clearfix"></div>
                </div> 
</div> 

