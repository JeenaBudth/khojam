-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2015 at 11:37 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upveda_rerl`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_publication`
--

CREATE TABLE IF NOT EXISTS `up_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `attachment1` varchar(255) DEFAULT NULL,
  `attachment2` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_publication`
--

INSERT INTO `up_publication` (`id`, `title`, `date`, `description`, `attachment1`, `attachment2`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'dipesh', '2015-12-09', 'test &nbsp;dkdkkd\r\n', '1attachment1.png', '1attachment2.pdf', 'dipesh', 2015, 2015, 'live'),
(6, 'dipesh manandhar', '2015-12-09', 'test', '6attachment1.png', '6attachment2.csv', 'dipesh-manandhar', 2015, 2015, 'live'),
(7, 'final', '2015-10-06', 'fdfdggdggggggggggf\r\n', '7attachment1.png', '7attachment2.pdf', 'final', 2015, 2015, 'live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
