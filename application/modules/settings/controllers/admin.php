<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('settings'); //module name is settings here	
	}
	
	function index()
	{	
		$data['cms'] = $this->get_data_from_db();
		
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['site_name'] = $this->input->post('site_name', TRUE);			
		$data['meta_topic'] = $this->input->post('meta_topic', TRUE);	
		$data['meta_data'] = $this->input->post('meta_data', TRUE);	
		$data['contact_email'] = $this->input->post('contact_email', TRUE);	
		$data['date_format'] = $this->input->post('date_format', TRUE);	
		$data['frontend_enabled'] = $this->input->post('frontend_enabled', TRUE);	
		$data['unavailable_message'] = $this->input->post('unavailable_message', TRUE);	
		//print_r($data);die();
		return $data;
	}
	
	function get_data_from_db()
	{
		$query = $this->get_settings();
		foreach($query->result() as $row)
		{
			$data['site_name'] = $row->site_name;
			$data['meta_topic'] = $row->meta_topic;
			$data['meta_data'] = $row->meta_data;
			$data['contact_email'] = $row->contact_email;
			$data['date_format'] = $row->date_format;			
			$data['frontend_enabled'] = $row->frontend_enabled;
			$data['unavailable_message'] = $row->unavailable_message;
			$data['favicon'] = $row->favicon;
			$data['logo'] = $row->logo;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	function get_settings()
	{
		$this->load->model('mdl_settings');
		$query = $this->mdl_settings->get_settings();
		return $query;
	}
	
	function submit()
	{	
		$data = $this->get_data_from_post();	
		$update_id = 1; //this always stays 1 because we have only 1 row for site settings		
		$this->_update($update_id, $data);		
		redirect('admin/settings');	
	}
	
	function submit_favicon()
	{			
		$attach = $this->get_attachment_from_db('favicon');
		$uploadattachment = $this->do_upload('favicon');
		$data['favicon'] = $uploadattachment['upload_data']['file_name'];
		if(empty($data['favicon'])){
			$data['favicon'] = $attach['favicon'];
		}
		
		$update_id = 1; //this always stays 1 because we have only 1 row for site settings		
		$this->_update($update_id, $data);	
		redirect('admin/settings/#tab_1_2');			
	}
	
	function submit_logo()
	{		
		$attach = $this->get_attachment_from_db('logo');
		$uploadattachment = $this->do_upload('logo');
		$data['logo'] = $uploadattachment['upload_data']['file_name'];
		if(empty($data['logo'])){
			$data['logo'] = $attach['logo'];
		}
		
		$update_id = 1; //this always stays 1 because we have only 1 row for site settings		
		$this->_update($update_id, $data);	
		redirect('admin/settings/#tab_1_3');		
	}
		
	function get_attachment_from_db($name)
	{
		$query = $this->get_where($name);
		foreach($query->result() as $row)
		{
			$data[$name] = $row->$name;				
		}
			return $data;
	}
	
	function do_upload($name) 
	{ 
	   $config['upload_path']   =   "./uploads/settings/"; 
	   $config['file_name'] = $name;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "png|ico";  
	   $config['max_size']      =   "5120"; //that's 5MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280";

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		 
		return $datas;
	}
	
	
	
	function get_where($name){
	$this->load->model('mdl_settings');
	$query = $this->mdl_settings->get_where($name);
	return $query;
	}

	function _update($id, $data){
	$this->load->model('mdl_settings');
	$this->mdl_settings->_update($id, $data);
	}
	
	
}