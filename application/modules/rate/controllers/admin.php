<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('rate'); //module name is banner here	
	}
	
	function index()
	{	
            die('index');		
	}
        
                
	
	function get_data_from_post()
	{
		$data['title'] = $this->input->post('title', TRUE);			
		$data['sub_title'] = $this->input->post('sub_title', TRUE);	
		$data['status'] = $this->input->post('status', TRUE);	
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
                                
				$data['attachment'] = $attach['attachment'];
                                
//                                var_dump($data);die('dgvdf');
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
			
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
			$data['sub_title'] = $row->sub_title;						
			$data['attachment'] = $row->attachment;
			$data['status'] = $row->status;		
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
//                                        var_dump($data);die();
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_banner');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/banner');
			}
		else
		{
			$this->mdl_banner->_delete($delete_id);
			redirect('admin/banner');
		}				
	}
	
	
	function submit()
	{		
	//no validation in banner because it has been created from jquery	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){						
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
//                                var_dump($data);die('gfdg');
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];}
				
				$this->_update($update_id, $data);
			} else {								
					
				$nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
//                                var_dump($data);die('njk');
				
				$this->_insert($data);	
			}
		
		redirect('admin/banner');	
			
	}
	
	
	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/banner/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_banner');
	$id = $this->mdl_banner->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_banner');
	$query = $this->mdl_banner->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_banner');
	$query = $this->mdl_banner->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_banner');
	$this->mdl_banner->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_banner');
	$this->mdl_banner->_update($id, $data);
       
	}
	
	function _delete($id){
	$this->load->model('mdl_banner');
	$this->mdl_banner->_delete($id);
	}
}