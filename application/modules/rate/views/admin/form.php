<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
                <link href="<?php echo base_url(); ?>design/frontend/css/rating.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>design/frontend/js/rating.js"></script>
<script language="javascript" type="text/javascript">
$(function() {
    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'postID'
    });
});

function processRating(val, attrVal){
alert(val);
    $.ajax({
        type: 'POST',
        url: <?php base_url()?>'rate/rating',
        data: 'postID='+attrVal+'&ratingPoints='+val,
        dataType: 'json',
        success : function(data) {
            if (data.status == 'ok') {
                alert('You have rated '+val+' to CodexWorld');
                $('#avgrat').text(data.average_rating);
                $('#totalrat').text(data.rating_number);
            }else{
                alert('Some problem occured, please try again.');
            }
        }
    });
}
</script>
<style type="text/css">
    .overall-rating{font-size: 14px;margin-top: 5px;color: #8e8d8d;}
</style>

<input name="rating" value="0" id="rating_star" type="hidden" postID="1" />
    <div class="overall-rating">(Average Rating <span id="avgrat"><?php echo $ratingRow['average_rating']; ?></span>
Based on <span id="totalrat"><?php echo $ratingRow['rating_number']; ?></span>  rating)</span></div>


            	<h4><i class="icon-reorder"></i> Banner</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo form_open_multipart('admin/banner/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title</label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('title', $title, 'class="form-control"');?>
						</div> 
                    </div>
                    
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Sub_Title</label> 
                        <div class="col-md-10">
                        	<?php echo form_input('sub_title', $sub_title, 'class="form-control"');?>
                        </div> 
					</div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php  if(!empty($update_id)){
										
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment
												);
										}else{
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment,
												'class' => 'required'
												);
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/banner/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>
                         
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div>  
                    			
                  
                
                    
                    <div class="form-actions"> 
						<?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>