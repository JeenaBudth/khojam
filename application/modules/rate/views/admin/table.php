<div class="manage">
    <input type="button" value="Add Content" id="create" onclick="location.href='<?php echo base_url()?>admin/banner/create';"/>  
</div>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> Banner </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Title</th>   
                    <th>Sub-title</th>  
                    <th>Attachment</th> 
                    <th>Status</th>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->title;?></td>  
                    <td><?php echo $row->sub_title;?></td>  
                    <td><img src="<?php echo base_url();?>uploads/banner/<?php echo $row->attachment;?>" style="height:50px;"/></td> 
                    <td><?php echo $row->status;?></td>
					<td class="edit">
                        <a href="<?php echo base_url()?>admin/banner/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>                   
                        &nbsp;&nbsp;/&nbsp;&nbsp; 
                        <a href="<?php echo base_url()?>admin/banner/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
					</td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->