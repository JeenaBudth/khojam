<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_embassy extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_embassy_of_nepal";
	return $table;
	} 

	function get($order_by){
	$table = $this->get_table();
        $this->db->select('up_embassy_of_nepal.*,district.dis_name_en as district');	
	$this->db->order_by($order_by,'ASC');
        $this->db->join('district','district.dis_id=up_embassy_of_nepal.district_id');
	$query=$this->db->get($table);
//        var_dump($query->result());die;
	return $query;
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
        
	return $query;
	}
	
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	
	
	$insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
	$permission_table = 'up_permissions';	
	$this->db->insert($permission_table, $insert_null_permission_array);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_embassy_of_nepal'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_modules')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_embassy_of_nepal')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
        
          function get_embassy_details_front() {
        $table = $this->get_table();
        $this->db->select('up_embassy_of_nepal.*,district.dis_name_en as district');	
	
        $this->db->join('district','district.dis_id=up_embassy_of_nepal.district_id');
        $this->db->where('up_embassy_of_nepal.status','live');
        $this->db->order_by('up_embassy_of_nepal.id', 'desc');
        $query = $this->db->get($table);
//        var_dump($query->result());die;
        return $query;
    }
        
	

}