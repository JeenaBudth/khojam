<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Content </h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/embassy_of_nepal/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control required"');?>
						</div> 
                    </div>
                 <div class="form-group"> 
                    	<label class="col-md-2 control-label">Number <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('number', $number, 'class="form-control required"');?>
						</div> 
                    </div>
                 <div class="form-group"> 
                    	<label class="col-md-2 control-label">Address <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('address', $address, 'class="form-control required"');?>
						</div> 
                    </div>
                
                <div class="form-group"> 
                    <label class="col-md-2 control-label">Meta_description </label> 
                    <div class="col-md-10"> 
                        <?php echo form_textarea('meta_description', $meta_description); ?>

                    </div> 
                </div>
                 <div class="form-group"> 
                    <label class="col-md-2 control-label">Search key </label> 
                    <div class="col-md-10"> 
                        <?php echo form_textarea('search_key', $search_key); ?>

                    </div> 
                </div>
                <div class="form-group"> 
                    <label class="col-md-2 control-label">District <span class="required">*</span></label> 
                    <div class="col-md-10"> 
                        <?php
                        $options = $all_district;
                        echo form_dropdown('district_id', $options, $district_id);
                        ?>
                    </div> 
                </div>
                
                  <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'live'    => 'live',
                                'draft'  => 'draft',
                              
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
			</div> 
                    </div>
                    
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>