<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('embassy_of_nepal'); //module name is embassy_of_nepal here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);
                $data['number'] = $this->input->post('number', TRUE);
                $data['address'] = $this->input->post('address', TRUE);
                 $data['meta_description'] = $this->input->post('meta_description', TRUE);
                  $data['search_key'] = $this->input->post('search_key', TRUE);
                $data['district_id'] = $this->input->post('district_id', TRUE);
                $data['slug'] = strtolower(url_title($data['name']));
                  $data['status']=$this->input->post('status',TRUE);
		
			$update_id = $this->input->post('update_id', TRUE);
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
                        $data['number'] = $row->number;
                        $data['address'] = $row->address;
                        $data['district_id'] = $row->district_id;
                        $data['meta_description'] = $row->meta_description;
                        $data['search_key'] = $row->search_key;
                        $data['status']=$row->status;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
                        $data['all_district'] = $this->get_district();
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
			
		
		
		
	}
	
	function get_district() {
        $this->load->model('district/mdl_district');
        $district_id = $this->mdl_district->get_district();
        return $district_id;
    }
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_telephone.name]'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			redirect('admin/embassy_of_nepal');
		}
			
	}
        function delete()
	{
              $this->load->model('mdl_embassy');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/embassy_of_nepal');
			}
		else
		{
			$this->mdl_embassy->_delete($delete_id);
			redirect('admin/embassy_of_nepal');
		}			
			
	}	
	
	
		

	function get($order_by){
	$this->load->model('mdl_embassy');
	$query = $this->mdl_embassy->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_embassy');
	$query = $this->mdl_embassy->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_embassy');
	$this->mdl_embassy->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_embassy');
	$this->mdl_embassy->_update($id, $data);
	}
        function _delete($id){
	$this->load->model('mdl_embassy');
	$this->mdl_users->_delete($id);
	}
}