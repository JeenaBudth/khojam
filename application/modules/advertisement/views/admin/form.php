<script src="<?php echo base_url();?>assets/tinymce/js/tinymce.min.js"></script>
<script>
    tinymce.init({
    selector: "textarea",
    theme: "modern",
    /*width: 300,
    height: 300,*/
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor filemanager"
   ],
   content_css: "content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 });

</script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $( document ).ready(function() {
        $("#cat_id").change(function () {
            var category= $("#cat_id").val();
          var district=$("#dis_id").val();
            if($("#adv_id" ).val()==3 && category!=0 && district!=0)
          get_organization_list();
      
      });
      $("#dis_id").change(function () {
          var category= $("#cat_id").val();
          var district=$("#dis_id").val();
          if($("#adv_id" ).val()==3 && category!=0 && district!=0)
          get_organization_list();
      });
      function get_organization_list(){
          $('#org_id').children().remove();
          var base_url = '<?php echo base_url() ?>';
         var category= $("#cat_id").val();
          var district=$("#dis_id").val();
          $.ajax({
                    type: 'POST',
                    url: base_url + 'organization/get_organization_ajax',
                    data: 'category=' + category + '&district=' + district 
                }).done(function (data) {
                        $("#org_id").append($('<option>', {
                        value: '',
                        text: '--Select--',
                    }));
                $.each($.parseJSON(data), function(k, v) {
                   
                          $("#org_id").append($('<option>', {
                        value: v.id,
                        text: v.name,
                    }));
                    
                });
                    });
                
      }
    $("#adv_id" ).change(function() {
        if($("#adv_id" ).val()==1){
         $(".cat_id" ) .show("slow");
         $(".dis_id" ).hide("slow");
         $(".org_id" ).hide("slow");
         $("#dis_id").val(0);
          $("#org_id").val(0);
        }
        else if($("#adv_id" ).val()==2){
         $(".cat_id" ) .show("slow");
         $(".dis_id" ).show("slow");
         $(".org_id" ).hide("slow");
         $("#org_id").val(0);
        }
         else if($("#adv_id" ).val()==3){
        $(".cat_id" ) .show("slow");
         $(".dis_id" ).show("slow");
         $(".org_id" ).show("slow");
        }
        else if($("#adv_id" ).val()==4){
          $(".cat_id" ) .hide("slow");
         $(".dis_id" ).hide("slow");
         $(".org_id" ).hide("slow"); 
         $("#cat_id").val(0);
         $("#dis_id").val(0);
         $("#org_id").val(0);
        }
        else{
           $(".cat_id" ) .hide("slow");
         $(".dis_id" ).hide("slow");
         $(".org_id" ).hide("slow"); 
          $("#cat_id").val(0);
           $("#dis_id").val(0);
            $("#org_id").val(0);
        }
        
  
});
});
   
    </script>
   

    
    
    
    
    
    
   
<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> About Us</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/advertisement/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group" > 
                    	<label class="col-md-2 control-label"> Advertisement Type <span class="required" >*</span></label> 
                    	<div class="col-md-10"> 
                           
                        	<?php 
                                 $options = array(
                                     '0'=>'Select',
                                        '1'  => 'Category',
                                        '2'    => 'District',
                                        '3'   => 'Organization',
                                        '4'=>'Search',
                                       
                                      );
                                echo form_dropdown('adv_id', $options,$adv_id, 'class="form-control required" id="adv_id"');?>
						</div> 
                    </div>
               

                 <div class="form-group dis_id " style="display: <?php echo($district_id==0 || $district_id=='')?'none':'block';?>">
            <label class="col-md-2 control-label">District </label>
            <div class="col-md-10"> 
                        	<?php $selected = $district_id; $options = $group_array_district;
				
		 			echo form_dropdown('district_id', $options, $selected, 'class="form-control" id="dis_id"')
                                ;?>		</div> 
          </div>
                 <div class="form-group cat_id " style="display: <?php echo($category_id==0 || $category_id=='')?'none':'block';?>">
                    <label class="col-md-2 control-label">Category </label>
                    <div class="col-md-10"> 
                                        <?php $selected = $category_id; $options = $group_array_category;

                                                echo form_dropdown('category_id', $options, $selected, 'class="form-control" id="cat_id"')
                                        ;?>		</div> 
                  </div>
                
                 <div class="form-group org_id" style="display: <?php echo($organization_id==0 || $organization_id=='')?'none':'block';?>">
                    <label class="col-md-2 control-label">Organization </label>
                    <div class="col-md-10"> 

                                      <?php $selected = $organization_id; $options = $group_array_organization;

                                              echo form_dropdown('organization_id', $options, $selected, 'class="form-control" id="org_id"')
                                      ;?>	</div> 
                  </div>
                
                             <div class="form-group"> 
                                    <label class="col-md-2 control-label">Link</label> 
                                    <div class="col-md-10">
                                            <?php echo form_input('link', $link, 'class="form-control required"');?>
                                    </div> 
                            </div>
<!--                 <div class="form-group"> 
                        <label class="col-md-2 control-label">Meta Search_keys</label> 
                        <div class="col-md-10">
                        	<?php echo form_input('search_keys', $search_keys, 'class="form-control required"');?>
                        </div> 
		</div>
                <div class="form-group"> 
                        <label class="col-md-2 control-label">Meta Description</label> 
                        <div class="col-md-10">
                        	<?php echo form_input('meta_description', $meta_description, 'class="form-control required"');?>
                        </div> 
		</div>-->
                
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Attachments (350*175) <span class="required">*</span></label> 
                        <div class="col-md-2"> 
								<?php  if(!empty($update_id)){
										
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
                                                                                                'value' => $attachment,
                                                                                                
												);
										}else{
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment,
												
											
                                                                                                );
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                                <img src="<?php echo base_url();?>uploads/advertisement/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                       
                    </div>
                       
                   
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div> 
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>