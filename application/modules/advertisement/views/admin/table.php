<div class="manage">
    <input type="button" value="Add Content" id="create" onclick="location.href='<?php echo base_url()?>admin/advertisement/create';"/> 
</div>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i>Add Advertisement</h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Advertisement Type</th>
                     <th data-class="expand">Link</th>
                    <th data-class="expand">Organization </th>
                    <th data-class="expand">District </th>
                    <th data-class="expand">Category </th>
                   
                     <th data-class="expand">Attachment </th>
<!--                    <th>Set Permission</th> -->
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php if($row->adv_id==1)
                              echo 'Category';
                              else if($row->adv_id==2)
                              echo 'District';
                              else if($row->adv_id==3)
                              echo 'Organization';
                          else {
                              echo 'Search';
                          }
                        
                         ?>
                    </td>
                    <td><?php echo Ucfirst($row->link);?></td>
                    <td><?php echo Ucfirst($row->org_name);?></td>
                    <td><?php echo Ucfirst($row->district_name);?></td>
                    <td><?php echo Ucfirst($row->cat_name);?></td>
                 
                   
                     <td><img src="<?php echo base_url();?>uploads/advertisement/<?php echo $row->attachment;?>" style="height:50px;"/></td>
                   
                    <td class="edit">
                            <a href="<?php echo base_url()?>admin/advertisement/view/<?php echo $row->id;?>"><i class="icon-eye-open"></i></a>
                            &nbsp;/&nbsp; 
                            <a href="<?php echo base_url()?>admin/advertisement/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>                   
                            &nbsp;/&nbsp; 
                            <a href="<?php echo base_url()?>admin/advertisement/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
                	</td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->