<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_advertisement extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_advertisement";
	return $table;
	} 
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	function get_details($slug){
        $table = 'up_advertisement';
	$this->db->where('slug', $slug);
	$query=$this->db->get($table);
	return $query;
        }
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	
	
	$insert_null_permission_array = array('group_id' => $next_id, 'roles' => 'a:0:{}');
	$permission_table = 'up_permissions';	
	$this->db->insert($permission_table, $insert_null_permission_array);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_advertisement'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_modules')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, title');
                //$this->db->where('id > 1');
		$this->db->order_by('id','AESC');
		$dropdowns = $this->db->get('up_advertisement')->result();
		foreach ($dropdowns as $dropdown)
		{
                //$dropdownlist[0] = '-- Select Panchakarma --';    
		$dropdownlist[$dropdown->id] = $dropdown->title;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
        function get_advertisement(){
//	$table = $this->get_table();
//	$this->db->order_by('id','ASC');
//        $this->db->where('status', 'live');
//	$query=$this->db->get($table)->result_array();
//     
//	return $query;
            
            $table = $this->get_table();
            $this->db->select('up_advertisement.*,district.dis_name_en as district, up_category.name as category');	

            $this->db->join('district','district.dis_id=up_advertisement.district_id');
             $this->db->join('up_category','up_category.id=up_advertisement.category_id');
            $this->db->where('up_advertisement.status','live');
            $this->db->order_by('up_advertisement.id', 'desc');
            $query = $this->db->get($table);
          
            return $query;
        
	}
           function get_country_dropdown()
	{
	$this->db->select('id, country_name');	
	$this->db->order_by('country_name');
	$dropdowns = $this->db->get('tbl_countries')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->country_name] = $dropdown->country_name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
        
        function get_search(){
	$table = $this->get_table();
        $this->db->select('up_advertisement.*');
	$this->db->where('adv_id', '4');
	$query=$this->db->get($table,3);
	return $query;
	}
        
         function get_category($cat_id){
//             echo $cat_id;die;
	$table = $this->get_table();
        $this->db->select('up_advertisement.*');
	$this->db->where('adv_id', '1');
        $this->db->where('category_id', $cat_id);
	$query=$this->db->get($table,3);
//        var_dump($query->result());
	return $query;
	}
        
         function get_district($cat_id,$dis_id){
	$table = $this->get_table();
        $this->db->select('up_advertisement.*');
	$this->db->where('adv_id', '2');
        $this->db->where('category_id', $cat_id);
        $this->db->where('district_id', $dis_id);
	$query=$this->db->get($table,3);
	return $query;
	}
         function get_organization($cat_id,$dis_id,$org_id){
	$table = $this->get_table();
        $this->db->select('up_advertisement.*');
	$this->db->where('adv_id', '3');
        $this->db->where('organization_id', $org_id);
        $this->db->where('category_id', $cat_id);
        $this->db->where('district_id', $dis_id);
	$query=$this->db->get($table,3);
	return $query;
	}

         function get_join()
        {
            $table = $this->get_table();
            $this->db->select('up_advertisement.*,up_organization.name as org_name, up_category.name as cat_name,district.dis_name_en as district_name');
             $this->db->join('up_category', 'up_advertisement.category_id=up_category.id','left outer');
            $this->db->join('up_organization', 'up_advertisement.organization_id=up_organization.id','left outer');
            $this->db->join('district', 'up_advertisement.district_id=district.dis_id','left outer');
//            $this->db->order_by($order_by);
            $query=$this->db->get($table);
//            var_dump($query->result);die;
            return $query;
        }
}