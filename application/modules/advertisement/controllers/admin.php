<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('advertisement'); //module name is groups here	
	}
	
	function index()
	{	
		//$data['query'] = $this->get('id');
                 $data['query']=  $this->get_join();
                
		$data['view_file'] ="admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	function get_data_from_post()
	{       
		$data['adv_id'] = $this->input->post('adv_id', TRUE);		
		$data['link'] = $this->input->post('link', TRUE);
                $data['organization_id'] = $this->input->post('organization_id', TRUE);
                $data['category_id'] = $this->input->post('category_id', TRUE);
                $data['district_id'] = $this->input->post('district_id', TRUE);
//                $data['slug'] = strtolower(url_title($data['title']));
//                $data['search_keys'] = $this->input->post('search_keys', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
//                $data['meta_description'] = $this->input->post('meta_description', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
                if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
                                //echo $data['attachment']; die;
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
//                        $data['group_array_organization'] = $this->get_groups_organization();
//                        $data['group_array_district'] = $this->get_groups_district();
//                        $data['group_array_category'] = $this->get_groups_category();
			$data['adv_id'] = $row->adv_id;
                        $data['link'] = $row->link;
                        $data['organization_id'] = $row->organization_id;
                        $data['district_id'] = $row->district_id;
                        $data['category_id'] = $row->category_id;
                        $data['attachment'] = $row->attachment;
                        $data['status'] = $row->status;
//                        $data['search_keys'] = $row->search_keys;
//                        $data['meta_description'] = $row->meta_description;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
                
		return $data;
	}
        function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
		//if($update_id != 1){
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
                                      //  var_dump($data);die;
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
                       
			$data['group_array_organization'] = $this->get_groups_organization();
                        $data['group_array_district'] = $this->get_groups_district();
                        $data['group_array_category'] = $this->get_groups_category();
//                         die('hjdfh');
//                        var_dump($data['group_array_category']); die;
			$data['update_id'] = $update_id;	
			$data['view_file'] = "admin/form";
                        
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
               // }
			
//		}else{
//		redirect('admin/groups');
//		}
	}
	
	
	
		
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('adv_id', 'Advertisementtype', 'required|xss_clean'); //we don't want unique_validation error while editing
		
                }
		else{
		$this->form_validation->set_rules('adv_id', 'Advertisementtype', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){	
                            $attach = $this->get_attachment_from_db($update_id);
                            $uploadattachment = $this->do_upload($update_id);
                            $data['attachment'] = $uploadattachment['upload_data']['file_name'];
                            if(empty($data['attachment'])){
                            $data['attachment'] = $attach['attachment'];}
                            $this->_update($update_id, $data);
                                
			} else {
                              $nextid = $this->get_id();
			      $uploadattachment = $this->do_upload($nextid);
			      $data['attachment'] = $uploadattachment['upload_data']['file_name'];
		              $this->_insert($data);
			}
			
			redirect('admin/advertisement');
		}
               }

	   function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/advertisement/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
		if ( ! $this->upload->do_upload())
		{
			echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
                    //$datas = array('upload_data' => $this->upload->data());	
		}
		else
		{
			echo 'File has been uploaded';
//                        var_dump($config);
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
        function get_id(){
	$this->load->model('mdl_advertisement');
	$id = $this->mdl_advertisement->get_id();
	return $id;
	}

	function get($id){
	$this->load->model('mdl_advertisement');
	$query = $this->mdl_advertisement->get($id);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_advertisement');
	$query = $this->mdl_advertisement->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_advertisement');
	$this->mdl_advertisement->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_advertisement');
	$this->mdl_advertisement->_update($id, $data);
	}
        function delete()
	{	$this->load->model('mdl_advertisement');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/advertisement');
			}
		else
		{
			$this->mdl_advertisement->_delete($delete_id);
			redirect('admin/advertisement');
		}			
			
	}
       function get_groups_organization()
	{
	$this->load->model('organization/mdl_organization');
	$query = $this->mdl_organization->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_groups_district()
	{
	$this->load->model('district/mdl_district');
	$query = $this->mdl_district->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_groups_category()
	{
	$this->load->model('category/mdl_category');
	$query = $this->mdl_category->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_organization_dropdown($cat_id,$dis_id)
        {
        $this->load->model('organization/mdl_organization');
	$query = $this->mdl_organization->get_organization_dropdown($cat_id,$dis_id); 
        return $qyery;
        }
        
         function get_join(){
        $this->load->model('mdl_advertisement');
        $query = $this->mdl_advertisement->get_join();
//        var_dump($query->result());die;
        return $query;
    }
}